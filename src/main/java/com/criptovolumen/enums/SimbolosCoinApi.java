package com.criptovolumen.enums;

import lombok.Getter;

public enum SimbolosCoinApi {

	OKCOIN_CNY_SPOT_BTC_CNY("OKCOIN_CNY_SPOT_BTC_CNY", "okcoinCnySpotBtcCny"), //
	HUOBI_SPOT_BTC_CNY("HUOBI_SPOT_BTC_CNY", "huobiSpotBtcCny"), //
	BTCCHINA_SPOT_BTC_CNY("BTCCHINA_SPOT_BTC_CNY", "btcchinaSpotBtcCny"), //
	COINCHECK_SPOT_BTC_JPY("COINCHECK_SPOT_BTC_JPY", "coincheckSpotBtcJpy"), //
	BITFINEX_SPOT_BTC_USD("BITFINEX_SPOT_BTC_USD", "bitfinexSpotBtcUsd"), //
	COINBASE_SPOT_BTC_USD("COINBASE_SPOT_BTC_USD", "coinbaseSpotBtcUsd"), //
	BTC_E_SPOT_BTC_USD("BTC-E_SPOT_BTC_USD", "btcESpotBtcUsd"), //
	BINANCE_SPOT_BTC_USDT("BINANCE_SPOT_BTC_USDT", "binanceSpotBtcUsdt"), //
	CHBTC_SPOT_BTC_CNY("CHBTC_SPOT_BTC_CNY", "chbtcSpotBtcCny"), //
	BITSTAMP_SPOT_BTC_USD("BITSTAMP_SPOT_BTC_USD", "bitstampSpotBtcUsd"), //
	KRAKEN_SPOT_BTC_EUR("KRAKEN_SPOT_BTC_EUR", "krakenSpotBtcEur"), //
	BITHUMB_SPOT_BTC_KRW("BITHUMB_SPOT_BTC_KRW", "bithumbSpotBtcKrw"), //
	BITFLYER_SPOT_BTC_JPY("BITFLYER_SPOT_BTC_JPY", "bitflyerSpotBtcJpy"), //
	POLONIEX_SPOT_BTC_USDT("POLONIEX_SPOT_BTC_USDT", "poloniexSpotBtcUsdt"), //
	BTCTRADE_SPOT_BTC_CNY("BTCTRADE_SPOT_BTC_CNY", "btctradeSpotBtcCny"), //
	COINBASE_SPOT_BTC_EUR("COINBASE_SPOT_BTC_EUR", "coinbaseSpotBtcEur"), //
	COINONE_SPOT_BTC_KRW("COINONE_SPOT_BTC_KRW", "coinoneSpotBtcKrw"), //
	OKCOIN_USD_SPOT_BTC_USD("OKCOIN_USD_SPOT_BTC_USD", "okcoinUsdSpotBtcUsd"), //
	KRAKEN_SPOT_BTC_USD("KRAKEN_SPOT_BTC_USD", "krakenSpotBtcUsd"), //
	ZAIF_SPOT_BTC_JPY("ZAIF_SPOT_BTC_JPY", "zaifSpotBtcJpy"), //
	MTGOX_SPOT_BTC_USD("MTGOX_SPOT_BTC_USD", "mtgoxSpotBtcUsd"), //
	BITTREX_SPOT_BTC_USDT("BITTREX_SPOT_BTC_USDT", "bittrexSpotBtcUsdt"), //
//	HUOBIPRO_SPOT_BTC_USDT("HUOBIPRO_SPOT_BTC_USDT", "huobiproSpotBtcUsdt"), //
	LAKEBTC_SPOT_BTC_USD("LAKEBTC_SPOT_BTC_USD", "lakebtcSpotBtcUsd"), //
	BITSTAMP_SPOT_BTC_EUR("BITSTAMP_SPOT_BTC_EUR", "bitstampSpotBtcEur"), //
	KORBIT_SPOT_BTC_KRW("KORBIT_SPOT_BTC_KRW", "korbitSpotBtcKrw"), //
	GEMINI_SPOT_BTC_USD("GEMINI_SPOT_BTC_USD", "geminiSpotBtcUsd"), //
	HITBTC_SPOT_BTC_USDT("HITBTC_SPOT_BTC_USDT", "hitbtcSpotBtcUsdt"), //
	EXMO_SPOT_BTC_USD("EXMO_SPOT_BTC_USD", "exmoSpotBtcUsd"), //
	ACX_SPOT_BTC_AUD("ACX_SPOT_BTC_AUD", "acxSpotBtcAud"), //
	COINBASE_SPOT_BTC_GBP("COINBASE_SPOT_BTC_GBP", "coinbaseSpotBtcGbp"), //
	OKEX_SPOT_BTC_USDT("OKEX_SPOT_BTC_USDT", "okexSpotBtcUsdt"), //
	EXMO_SPOT_BTC_RUB("EXMO_SPOT_BTC_RUB", "exmoSpotBtcRub"), //
	ITBIT_SPOT_BTC_USD("ITBIT_SPOT_BTC_USD", "itbitSpotBtcUsd"), //
	MIXCOINS_SPOT_BTC_USD("MIXCOINS_SPOT_BTC_USD", "mixcoinsSpotBtcUsd"), //
	VIRWOX_SPOT_BTC_SLL("VIRWOX_SPOT_BTC_SLL", "virwoxSpotBtcSll"), //
	MERCADOBITCOIN_SPOT_BTC_BRL("MERCADOBITCOIN_SPOT_BTC_BRL", "mercadobitcoinSpotBtcBrl"), //
	BTC_E_SPOT_BTC_RUB("BTC-E_SPOT_BTC_RUB", "btcESpotBtcRub"), //
	BITMARKET_SPOT_BTC_PLN("BITMARKET_SPOT_BTC_PLN", "bitmarketSpotBtcPln"), //
	LIQUI_SPOT_BTC_USDT("LIQUI_SPOT_BTC_USDT", "liquiSpotBtcUsdt"), //
	BITCOINID_SPOT_BTC_IDR("BITCOINID_SPOT_BTC_IDR", "bitcoinidSpotBtcIdr"), //
	BITSO_SPOT_BTC_MXN("BITSO_SPOT_BTC_MXN", "bitsoSpotBtcMxn"), //
	BTCBOX_SPOT_BTC_JPY("BTCBOX_SPOT_BTC_JPY", "btcboxSpotBtcJpy"), //
	CEXIO_SPOT_BTC_USD("CEXIO_SPOT_BTC_USD", "cexioSpotBtcUsd"), //
	QUOINE_SPOT_BTC_JPY("QUOINE_SPOT_BTC_JPY", "quoineSpotBtcJpy"), //
	LUNO_SPOT_BTC_ZAR("LUNO_SPOT_BTC_ZAR", "lunoSpotBtcZar"), //
	THEROCKTRADING_SPOT_BTC_EUR("THEROCKTRADING_SPOT_BTC_EUR", "therocktradingSpotBtcEur"), //
	CRYPTOPIA_SPOT_BTC_USDT("CRYPTOPIA_SPOT_BTC_USDT", "cryptopiaSpotBtcUsdt"), //
	BITFINEX_SPOT_BTC_EUR("BITFINEX_SPOT_BTC_EUR", "bitfinexSpotBtcEur"), //
	BTC_E_SPOT_BTC_EUR("BTC-E_SPOT_BTC_EUR", "btcESpotBtcEur"), //
	WEXNZ_SPOT_BTC_USD("WEXNZ_SPOT_BTC_USD", "wexnzSpotBtcUsd"), //
	QUADRIGACX_SPOT_BTC_CAD("QUADRIGACX_SPOT_BTC_CAD", "quadrigacxSpotBtcCad"), //
	MTGOX_SPOT_BTC_EUR("MTGOX_SPOT_BTC_EUR", "mtgoxSpotBtcEur"), //
	LAKEBTC_SPOT_BTC_JPY("LAKEBTC_SPOT_BTC_JPY", "lakebtcSpotBtcJpy"), //
	GATEIO_SPOT_BTC_USDT("GATEIO_SPOT_BTC_USDT", "gateioSpotBtcUsdt"), //
	QUOINE_SPOT_BTC_USD("QUOINE_SPOT_BTC_USD", "quoineSpotBtcUsd"), //
	EXMO_SPOT_BTC_EUR("EXMO_SPOT_BTC_EUR", "exmoSpotBtcEur"), //
	EXMO_SPOT_BTC_UAH("EXMO_SPOT_BTC_UAH", "exmoSpotBtcUah"), //
	LAKEBTC_SPOT_BTC_EUR("LAKEBTC_SPOT_BTC_EUR", "lakebtcSpotBtcEur"), //
	LAKEBTC_SPOT_BTC_CAD("LAKEBTC_SPOT_BTC_CAD", "lakebtcSpotBtcCad"), //
	COINFLOOR_SPOT_BTC_GBP("COINFLOOR_SPOT_BTC_GBP", "coinfloorSpotBtcGbp"), //
	HITBTC_SPOT_BTC_EUR("HITBTC_SPOT_BTC_EUR", "hitbtcSpotBtcEur"), //
	LIVECOIN_SPOT_BTC_USD("LIVECOIN_SPOT_BTC_USD", "livecoinSpotBtcUsd"), //
	YOBIT_SPOT_BTC_RUB("YOBIT_SPOT_BTC_RUB", "yobitSpotBtcRub"), //
	YOBIT_SPOT_BTC_USD("YOBIT_SPOT_BTC_USD", "yobitSpotBtcUsd"), //
	BTER_SPOT_BTC_CNY("BTER_SPOT_BTC_CNY", "bterSpotBtcCny"), //
	LAKEBTC_SPOT_BTC_GBP("LAKEBTC_SPOT_BTC_GBP", "lakebtcSpotBtcGbp"), //
	COINMATE_SPOT_BTC_EUR("COINMATE_SPOT_BTC_EUR", "coinmateSpotBtcEur"), //
	COINMATE_SPOT_BTC_CZK("COINMATE_SPOT_BTC_CZK", "coinmateSpotBtcCzk"), //
	BTCMARKETS_SPOT_BTC_AUD("BTCMARKETS_SPOT_BTC_AUD", "btcmarketsSpotBtcAud"), //
	KRAKEN_SPOT_BTC_JPY("KRAKEN_SPOT_BTC_JPY", "krakenSpotBtcJpy"), //
	KRAKEN_SPOT_BTC_CAD("KRAKEN_SPOT_BTC_CAD", "krakenSpotBtcCad"), //
	BTCTURK_SPOT_BTC_TRY("BTCTURK_SPOT_BTC_TRY", "btcturkSpotBtcTry"), //
	MTGOX_SPOT_BTC_GBP("MTGOX_SPOT_BTC_GBP", "mtgoxSpotBtcGbp"), //
	BXINTH_SPOT_BTC_THB("BXINTH_SPOT_BTC_THB", "bxinthSpotBtcThb"), //
	LUNO_SPOT_BTC_NGN("LUNO_SPOT_BTC_NGN", "lunoSpotBtcNgn"), //
	EXMO_SPOT_BTC_USDT("EXMO_SPOT_BTC_USDT", "exmoSpotBtcUsdt"), //
	GATECOIN_SPOT_BTC_USD("GATECOIN_SPOT_BTC_USD", "gatecoinSpotBtcUsd"), //
	WEXNZ_SPOT_BTC_RUB("WEXNZ_SPOT_BTC_RUB", "wexnzSpotBtcRub"), //
	ITBIT_SPOT_BTC_EUR("ITBIT_SPOT_BTC_EUR", "itbitSpotBtcEur"), //
	INDEPENDENTRESERVE_SPOT_BTC_AUD("INDEPENDENTRESERVE_SPOT_BTC_AUD", "independentreserveSpotBtcAud"), //
	QUOINE_SPOT_BTC_SGD("QUOINE_SPOT_BTC_SGD", "quoineSpotBtcSgd"), //
	GATECOIN_SPOT_BTC_HKD("GATECOIN_SPOT_BTC_HKD", "gatecoinSpotBtcHkd"), //
	KUCOIN_SPOT_BTC_USDT("KUCOIN_SPOT_BTC_USDT", "kucoinSpotBtcUsdt"), //
	GATECOIN_SPOT_BTC_EUR("GATECOIN_SPOT_BTC_EUR", "gatecoinSpotBtcEur"), //
	LUNO_SPOT_BTC_MYR("LUNO_SPOT_BTC_MYR", "lunoSpotBtcMyr"), //
	POLONIEX_SPOT_MYR_BTC("POLONIEX_SPOT_MYR_BTC", "poloniexSpotMyrBtc"), //
	MTGOX_SPOT_BTC_JPY("MTGOX_SPOT_BTC_JPY", "mtgoxSpotBtcJpy"), //
	CEXIO_SPOT_BTC_EUR("CEXIO_SPOT_BTC_EUR", "cexioSpotBtcEur"), //
	DSX_SPOT_BTC_USD("DSX_SPOT_BTC_USD", "dsxSpotBtcUsd"), //
	KRAKEN_SPOT_BTC_GBP("KRAKEN_SPOT_BTC_GBP", "krakenSpotBtcGbp"), //
	WEXNZ_SPOT_BTC_EUR("WEXNZ_SPOT_BTC_EUR", "wexnzSpotBtcEur"), //
	COINBASE_SPOT_BTC_CAD("COINBASE_SPOT_BTC_CAD", "coinbaseSpotBtcCad"), //
	CCEX_SPOT_USD_BTC("CCEX_SPOT_USD_BTC", "ccexSpotUsdBtc"), //
	THEROCKTRADING_SPOT_BTC_USD("THEROCKTRADING_SPOT_BTC_USD", "therocktradingSpotBtcUsd"), //
	ITBIT_SPOT_BTC_SGD("ITBIT_SPOT_BTC_SGD", "itbitSpotBtcSgd"), //
	MTGOX_SPOT_BTC_AUD("MTGOX_SPOT_BTC_AUD", "mtgoxSpotBtcAud"), //
	COINNEST_SPOT_BTC_KRW("COINNEST_SPOT_BTC_KRW", "coinnestSpotBtcKrw"), //
	TIDEX_SPOT_BTC_USDT("TIDEX_SPOT_BTC_USDT", "tidexSpotBtcUsdt"), //
	MTGOX_SPOT_BTC_PLN("MTGOX_SPOT_BTC_PLN", "mtgoxSpotBtcPln"), //
	BTCX_SPOT_BTC_USD("BTCX_SPOT_BTC_USD", "btcxSpotBtcUsd"), //
	BITTREX_SPOT_MYR_BTC("BITTREX_SPOT_MYR_BTC", "bittrexSpotMyrBtc"), //
	LAKEBTC_SPOT_BTC_AUD("LAKEBTC_SPOT_BTC_AUD", "lakebtcSpotBtcAud"), //
	LAKEBTC_SPOT_BTC_SGD("LAKEBTC_SPOT_BTC_SGD", "lakebtcSpotBtcSgd"), //
	QUOINE_SPOT_BTC_EUR("QUOINE_SPOT_BTC_EUR", "quoineSpotBtcEur"), //
	QUOINE_SPOT_BTC_AUD("QUOINE_SPOT_BTC_AUD", "quoineSpotBtcAud"), //
	BITTREX_SPOT_TUSD_BTC("BITTREX_SPOT_TUSD_BTC", "bittrexSpotTusdBtc"), //
	BTCX_SPOT_BTC_EUR("BTCX_SPOT_BTC_EUR", "btcxSpotBtcEur"), //
	KUNA_SPOT_BTC_UAH("KUNA_SPOT_BTC_UAH", "kunaSpotBtcUah"), //
	DSX_SPOT_BTC_EUR("DSX_SPOT_BTC_EUR", "dsxSpotBtcEur"), //
	LUNO_SPOT_BTC_IDR("LUNO_SPOT_BTC_IDR", "lunoSpotBtcIdr"), //
	BITMARKET_SPOT_BTC_EUR("BITMARKET_SPOT_BTC_EUR", "bitmarketSpotBtcEur"), //
	BTCTRADEUA_SPOT_BTC_UAH("BTCTRADEUA_SPOT_BTC_UAH", "btctradeuaSpotBtcUah"), //
	QUADRIGACX_SPOT_BTC_USD("QUADRIGACX_SPOT_BTC_USD", "quadrigacxSpotBtcUsd"), //
	ABUCOINS_SPOT_BTC_PLN("ABUCOINS_SPOT_BTC_PLN", "abucoinsSpotBtcPln"), //
	FYBSG_SPOT_BTC_SGD("FYBSG_SPOT_BTC_SGD", "fybsgSpotBtcSgd"), //
	CRYPTOPIA_SPOT_TOP_BTC("CRYPTOPIA_SPOT_TOP_BTC", "cryptopiaSpotTopBtc"), //
	QUOINE_SPOT_BTC_HKD("QUOINE_SPOT_BTC_HKD", "quoineSpotBtcHkd"), //
	QUOINE_SPOT_BTC_PHP("QUOINE_SPOT_BTC_PHP", "quoineSpotBtcPhp"), //
	QUOINE_SPOT_BTC_IDR("QUOINE_SPOT_BTC_IDR", "quoineSpotBtcIdr"), //
	LIVECOIN_SPOT_BTC_RUB("LIVECOIN_SPOT_BTC_RUB", "livecoinSpotBtcRub"), //
	BITKONAN_SPOT_BTC_USD("BITKONAN_SPOT_BTC_USD", "bitkonanSpotBtcUsd"), //
	FYBSG_SPOT_BTC_SEK("FYBSG_SPOT_BTC_SEK", "fybsgSpotBtcSek"), //
	EXMO_SPOT_BTC_PLN("EXMO_SPOT_BTC_PLN", "exmoSpotBtcPln"), //
	MTGOX_SPOT_BTC_CAD("MTGOX_SPOT_BTC_CAD", "mtgoxSpotBtcCad"), //
	LIVECOIN_SPOT_BTC_EUR("LIVECOIN_SPOT_BTC_EUR", "livecoinSpotBtcEur"), //
	BRAZILIEX_SPOT_BTC_BRL("BRAZILIEX_SPOT_BTC_BRL", "braziliexSpotBtcBrl"), //
	CEXIO_SPOT_BTC_GBP("CEXIO_SPOT_BTC_GBP", "cexioSpotBtcGbp"), //
	COINFLOOR_SPOT_BTC_USD("COINFLOOR_SPOT_BTC_USD", "coinfloorSpotBtcUsd"), //
	FYBSE_SPOT_BTC_SEK("FYBSE_SPOT_BTC_SEK", "fybseSpotBtcSek"), //
	INDEPENDENTRESERVE_SPOT_BTC_NZD("INDEPENDENTRESERVE_SPOT_BTC_NZD", "independentreserveSpotBtcNzd"), //
	BTCC_SPOT_BTC_USD("BTCC_SPOT_BTC_USD", "btccSpotBtcUsd"), //
	ABUCOINS_SPOT_BTC_EUR("ABUCOINS_SPOT_BTC_EUR", "abucoinsSpotBtcEur"), //
	BTC_E_SPOT_BTC_GBP("BTC-E_SPOT_BTC_GBP", "btcESpotBtcGbp"), //
	INDEPENDENTRESERVE_SPOT_BTC_USD("INDEPENDENTRESERVE_SPOT_BTC_USD", "independentreserveSpotBtcUsd"), //
	CEXIO_SPOT_BTC_RUB("CEXIO_SPOT_BTC_RUB", "cexioSpotBtcRub"), //
	BTC_E_SPOT_BTC_CNY("BTC-E_SPOT_BTC_CNY", "btcESpotBtcCny"), //
	SOUTHXCHANGE_SPOT_BTC_USD("SOUTHXCHANGE_SPOT_BTC_USD", "southxchangeSpotBtcUsd"), //
	MTGOX_SPOT_BTC_SEK("MTGOX_SPOT_BTC_SEK", "mtgoxSpotBtcSek"), //
	ABUCOINS_SPOT_BTC_USD("ABUCOINS_SPOT_BTC_USD", "abucoinsSpotBtcUsd"), //
	YUNBI_SPOT_BTC_CNY("YUNBI_SPOT_BTC_CNY", "yunbiSpotBtcCny"), //
	COINSECURE_SPOT_BTC_INR("COINSECURE_SPOT_BTC_INR", "coinsecureSpotBtcInr"), //
	YOBIT_SPOT_MVR_BTC("YOBIT_SPOT_MVR_BTC", "yobitSpotMvrBtc"), //
	MTGOX_SPOT_BTC_CHF("MTGOX_SPOT_BTC_CHF", "mtgoxSpotBtcChf"), //
	DSX_SPOT_BTC_RUB("DSX_SPOT_BTC_RUB", "dsxSpotBtcRub"), //
	YOBIT_SPOT_SDG_BTC("YOBIT_SPOT_SDG_BTC", "yobitSpotSdgBtc"), //
	QUOINE_SPOT_BTC_INR("QUOINE_SPOT_BTC_INR", "quoineSpotBtcInr"), //
	QUOINE_SPOT_BTC_CNY("QUOINE_SPOT_BTC_CNY", "quoineSpotBtcCny"), //
	MTGOX_SPOT_BTC_RUB("MTGOX_SPOT_BTC_RUB", "mtgoxSpotBtcRub"), //
	MTGOX_SPOT_BTC_CNY("MTGOX_SPOT_BTC_CNY", "mtgoxSpotBtcCny"), //
	CEXIO_SPOT_GHS_BTC("CEXIO_SPOT_GHS_BTC", "cexioSpotGhsBtc"), //
	CCEX_SPOT_USDT_BTC("CCEX_SPOT_USDT_BTC", "ccexSpotUsdtBtc"), //
	MTGOX_SPOT_BTC_NZD("MTGOX_SPOT_BTC_NZD", "mtgoxSpotBtcNzd"), //
	YOBIT_SPOT_PEN_BTC("YOBIT_SPOT_PEN_BTC", "yobitSpotPenBtc"), //
	YOBIT_SPOT_GHS_BTC("YOBIT_SPOT_GHS_BTC", "yobitSpotGhsBtc"), //
	COINFLOOR_SPOT_BTC_PLN("COINFLOOR_SPOT_BTC_PLN", "coinfloorSpotBtcPln"), //
	MTGOX_SPOT_BTC_SGD("MTGOX_SPOT_BTC_SGD", "mtgoxSpotBtcSgd"), //
	DSX_SPOT_BTC_GBP("DSX_SPOT_BTC_GBP", "dsxSpotBtcGbp"), //
	YOBIT_SPOT_MAD_BTC("YOBIT_SPOT_MAD_BTC", "yobitSpotMadBtc"), //
	YOBIT_SPOT_BAM_BTC("YOBIT_SPOT_BAM_BTC", "yobitSpotBamBtc"), //
	MTGOX_SPOT_BTC_DKK("MTGOX_SPOT_BTC_DKK", "mtgoxSpotBtcDkk"), //
	SOUTHXCHANGE_SPOT_BHD_BTC("SOUTHXCHANGE_SPOT_BHD_BTC", "southxchangeSpotBhdBtc"), //
	BITLISH_SPOT_BTC_EUR("BITLISH_SPOT_BTC_EUR", "bitlishSpotBtcEur"), //
	COINGI_SPOT_BTC_USD("COINGI_SPOT_BTC_USD", "coingiSpotBtcUsd"), //
	COINFLOOR_SPOT_BTC_EUR("COINFLOOR_SPOT_BTC_EUR", "coinfloorSpotBtcEur"), //
	BITLISH_SPOT_BTC_USD("BITLISH_SPOT_BTC_USD", "bitlishSpotBtcUsd"), //
	BITLISH_SPOT_BTC_RUB("BITLISH_SPOT_BTC_RUB", "bitlishSpotBtcRub"), //
	MTGOX_SPOT_BTC_THB("MTGOX_SPOT_BTC_THB", "mtgoxSpotBtcThb"), //
	COINGI_SPOT_BTC_EUR("COINGI_SPOT_BTC_EUR", "coingiSpotBtcEur"), //
	VBTC_SPOT_BTC_CLP("VBTC_SPOT_BTC_CLP", "vbtcSpotBtcClp"), //
	VBTC_SPOT_BTC_VEF("VBTC_SPOT_BTC_VEF", "vbtcSpotBtcVef"), //
	VBTC_SPOT_BTC_VND("VBTC_SPOT_BTC_VND", "vbtcSpotBtcVnd"), //
	VBTC_SPOT_BTC_BRL("VBTC_SPOT_BTC_BRL", "vbtcSpotBtcBrl"), //
	VBTC_SPOT_BTC_PKR("VBTC_SPOT_BTC_PKR", "vbtcSpotBtcPkr"), //
	FOXBIT_SPOT_BTC_BRL("FOXBIT_SPOT_BTC_BRL", "foxbitSpotBtcBrl"), //
	SURBITCOIN_SPOT_BTC_VEF("SURBITCOIN_SPOT_BTC_VEF", "surbitcoinSpotBtcVef"), //
	KRAKEN_SPOT_BTC_KRW("KRAKEN_SPOT_BTC_KRW", "krakenSpotBtcKrw"), //
	BITBAY_SPOT_BTC_USD("BITBAY_SPOT_BTC_USD", "bitbaySpotBtcUsd"), //
	BITBAY_SPOT_BTC_EUR("BITBAY_SPOT_BTC_EUR", "bitbaySpotBtcEur"), //
	ACX_SPOT_BTC_USD("ACX_SPOT_BTC_USD", "acxSpotBtcUsd"), //
	BITBAY_SPOT_BTC_PLN("BITBAY_SPOT_BTC_PLN", "bitbaySpotBtcPln"), //
	BTER_SPOT_BTC_USD("BTER_SPOT_BTC_USD", "bterSpotBtcUsd"), //
	LAKEBTC_SPOT_BTC_HKD("LAKEBTC_SPOT_BTC_HKD", "lakebtcSpotBtcHkd"), //
	XBTCE_SPOT_BTC_RUB("XBTCE_SPOT_BTC_RUB", "xbtceSpotBtcRub"), //
	XBTCE_SPOT_BTC_JPY("XBTCE_SPOT_BTC_JPY", "xbtceSpotBtcJpy"), //
	XBTCE_SPOT_BTC_USD("XBTCE_SPOT_BTC_USD", "xbtceSpotBtcUsd"), //
	XBTCE_SPOT_BTC_EUR("XBTCE_SPOT_BTC_EUR", "xbtceSpotBtcEur"), //
	XBTCE_SPOT_BTC_GBP("XBTCE_SPOT_BTC_GBP", "xbtceSpotBtcGbp"), //
	LAKEBTC_SPOT_BTC_SEK("LAKEBTC_SPOT_BTC_SEK", "lakebtcSpotBtcSek"), //
	LAKEBTC_SPOT_BTC_KRW("LAKEBTC_SPOT_BTC_KRW", "lakebtcSpotBtcKrw"), //
	CRYPTOPIA_SPOT_ETB_BTC("CRYPTOPIA_SPOT_ETB_BTC", "cryptopiaSpotEtbBtc"), //
	XBTCE_SPOT_BTC_INR("XBTCE_SPOT_BTC_INR", "xbtceSpotBtcInr"), //
	ANXPRO_SPOT_BTC_USD("ANXPRO_SPOT_BTC_USD", "anxproSpotBtcUsd"), //
	ANXPRO_SPOT_BTC_AUD("ANXPRO_SPOT_BTC_AUD", "anxproSpotBtcAud"), //
	ANXPRO_SPOT_BTC_CAD("ANXPRO_SPOT_BTC_CAD", "anxproSpotBtcCad"), //
	ANXPRO_SPOT_BTC_EUR("ANXPRO_SPOT_BTC_EUR", "anxproSpotBtcEur"), //
	LYKKE_SPOT_BTC_CHF("LYKKE_SPOT_BTC_CHF", "lykkeSpotBtcChf"), //
	LYKKE_SPOT_BTC_EUR("LYKKE_SPOT_BTC_EUR", "lykkeSpotBtcEur"), //
	ANXPRO_SPOT_BTC_GBP("ANXPRO_SPOT_BTC_GBP", "anxproSpotBtcGbp"), //
	LYKKE_SPOT_BTC_GBP("LYKKE_SPOT_BTC_GBP", "lykkeSpotBtcGbp"), //
	ANXPRO_SPOT_BTC_HKD("ANXPRO_SPOT_BTC_HKD", "anxproSpotBtcHkd"), //
	LYKKE_SPOT_BTC_JPY("LYKKE_SPOT_BTC_JPY", "lykkeSpotBtcJpy"), //
	ANXPRO_SPOT_BTC_JPY("ANXPRO_SPOT_BTC_JPY", "anxproSpotBtcJpy"), //
	ANXPRO_SPOT_BTC_NZD("ANXPRO_SPOT_BTC_NZD", "anxproSpotBtcNzd"), //
	LYKKE_SPOT_BTC_USD("LYKKE_SPOT_BTC_USD", "lykkeSpotBtcUsd"), //
	ANXPRO_SPOT_BTC_SGD("ANXPRO_SPOT_BTC_SGD", "anxproSpotBtcSgd"), //
	COINEXCHANGE_SPOT_RUB_BTC("COINEXCHANGE_SPOT_RUB_BTC", "coinexchangeSpotRubBtc"), //
	BITFINEX_SPOT_BTC_GBP("BITFINEX_SPOT_BTC_GBP", "bitfinexSpotBtcGbp"), //
	BITFINEX_SPOT_BTC_JPY("BITFINEX_SPOT_BTC_JPY", "bitfinexSpotBtcJpy"), //
	YOBIT_SPOT_USDT_BTC("YOBIT_SPOT_USDT_BTC", "yobitSpotUsdtBtc"), //
	SOUTHXCHANGE_SPOT_BTC_TUSD("SOUTHXCHANGE_SPOT_BTC_TUSD", "southxchangeSpotBtcTusd"), //
	BRAZILIEX_SPOT_USDT_BTC("BRAZILIEX_SPOT_USDT_BTC", "braziliexSpotUsdtBtc"), //
	BRAZILIEX_SPOT_BTC_USDT("BRAZILIEX_SPOT_BTC_USDT", "braziliexSpotBtcUsdt"), //
	BLEUTRADE_SPOT_USDT_BTC("BLEUTRADE_SPOT_USDT_BTC", "bleutradeSpotUsdtBtc"), //
	BLEUTRADE_SPOT_BTC_USDT("BLEUTRADE_SPOT_BTC_USDT", "bleutradeSpotBtcUsdt"), //
	BINANCE_SPOT_TUSD_BTC("BINANCE_SPOT_TUSD_BTC", "binanceSpotTusdBtc"), //
	CRYPTOPIA_SPOT_TUSD_BTC("CRYPTOPIA_SPOT_TUSD_BTC", "cryptopiaSpotTusdBtc"), //
	BINANCE_SPOT_BTC_TUSD("BINANCE_SPOT_BTC_TUSD", "binanceSpotBtcTusd"), //
	BITTREX_SPOT_BTC_USD("BITTREX_SPOT_BTC_USD", "bittrexSpotBtcUsd"), //
	LIVECOIN_SPOT_PLN_BTC("LIVECOIN_SPOT_PLN_BTC", "livecoinSpotPlnBtc"), //
	BITSO_SPOT_TUSD_BTC("BITSO_SPOT_TUSD_BTC", "bitsoSpotTusdBtc"), //
	DSX_SPOT_BTC_USDT("DSX_SPOT_BTC_USDT", "dsxSpotBtcUsdt"), //
	YOBIT_SPOT_TUSD_BTC("YOBIT_SPOT_TUSD_BTC", "yobitSpotTusdBtc"), //
	HITBTC_SPOT_BTC_TUSD("HITBTC_SPOT_BTC_TUSD", "hitbtcSpotBtcTusd"), //
	WEXNZ_SPOT_BTC_USDT("WEXNZ_SPOT_BTC_USDT", "wexnzSpotBtcUsdt"), //
	ACX_SPOT_BTC_USDT("ACX_SPOT_BTC_USDT", "acxSpotBtcUsdt"), //
	UPBIT_SPOT_TUSD_BTC("UPBIT_SPOT_TUSD_BTC", "upbitSpotTusdBtc"), //
	UPBIT_SPOT_BTC_KRW("UPBIT_SPOT_BTC_KRW", "upbitSpotBtcKrw"), //
	UPBIT_SPOT_BTC_USDT("UPBIT_SPOT_BTC_USDT", "upbitSpotBtcUsdt"), //
	BITLISH_SPOT_BTC_GBP("BITLISH_SPOT_BTC_GBP", "bitlishSpotBtcGbp"), //
	BITLISH_SPOT_BTC_JPY("BITLISH_SPOT_BTC_JPY", "bitlishSpotBtcJpy"), //
	BITLISH_SPOT_BTC_USDT("BITLISH_SPOT_BTC_USDT", "bitlishSpotBtcUsdt"), //
	BITBANK_SPOT_BTC_JPY("BITBANK_SPOT_BTC_JPY", "bitbankSpotBtcJpy"), //
	BITZ_SPOT_BTC_USDT("BITZ_SPOT_BTC_USDT", "bitzSpotBtcUsdt"), //
	EXMO_SPOT_BTC_TRY("EXMO_SPOT_BTC_TRY", "exmoSpotBtcTry");//

@Getter
private String codigo;

	@Getter
	private String propiedadVolumen;

	private SimbolosCoinApi(String codigo, String propiedadVolumen) {
		this.codigo = codigo;
		this.propiedadVolumen = propiedadVolumen;
	}
}
