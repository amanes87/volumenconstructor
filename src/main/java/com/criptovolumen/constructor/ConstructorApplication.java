package com.criptovolumen.constructor;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.coinapi.servicios.REST_methods;
import com.criptovolumen.Constantes;
import com.criptovolumen.enums.SimbolosCoinApi;
import com.criptovolumen.servicios.temporalidades.RecolectorVelasPasadasSv;

@SpringBootApplication
@ComponentScan({"com.criptovolumen"})
@EntityScan("com.criptovolumen")
@EnableJpaRepositories("com.criptovolumen")
public class ConstructorApplication  implements CommandLineRunner {

	@Autowired
	HiloExchanges hiloExchanges;
	
	@Autowired
	RecolectorVelasPasadasSv recolectorVelasPasadasSv;
	
	Logger logger = LoggerFactory.getLogger(ConstructorApplication.class);

	
	public static void main(String[] args) {
		SpringApplication.run(ConstructorApplication.class, args);

	}
	
	@Override
	public void run(String... args) throws Exception {
		if(args.length > 0) {
			REST_methods coinApi = new REST_methods(Constantes.API_KEY_COINAPI);
			int peticionesRestantes = coinApi.llamadasRestantes(Constantes.MAXIMO_PETICIONES_DIARIAS);
			coinApi.close();
			ParametrosRecolector parametros = new ParametrosRecolector(peticionesRestantes);
			if(args.length > 1) {
				try {
					parametros = new ParametrosRecolector(args);
				}catch(Exception e) {
					logger.error("Parametros mal formateados, revisar: {}", args.toString());
					System.exit(1);
				}
			}
			if(parametros.getPeticiones() > peticionesRestantes) {
				logger.info("Número de peticiones restantes de hoy {}", peticionesRestantes);
				logger.error("Número de velas solicitadas {} supera las de restantes de hoy {}", parametros.getPeticiones(), peticionesRestantes / SimbolosCoinApi.values().length * Constantes.COSTE_NUMERO_VELAS_PETICION);
				logger.info("Reduce el limite de peticiones o el rango de fechas");
				System.exit(1);
			}else {
				logger.info("Número de peticiones restantes de hoy {}", peticionesRestantes);
				logger.info("Número de velas que se recogerán {}", parametros.getLimite());
				logger.info("Desde el día {} hasta el día {}", parametros.getInicioRecoleccion(), parametros.getFinRecoleccion());
				logger.info("================={}=================", LocalDateTime.now());
				recolectorVelasPasadasSv.recolectarVelasPasadas(parametros);
				System.exit(0);
			}
		}else {
			logger.info("INICIO APLICACION MODO HILO");
			hiloExchanges.ejecutar(args);
		}
	}

}
