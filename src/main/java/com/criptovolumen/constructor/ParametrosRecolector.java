package com.criptovolumen.constructor;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import com.criptovolumen.Constantes;
import com.criptovolumen.enums.SimbolosCoinApi;

import lombok.Getter;
import lombok.Setter;

public class ParametrosRecolector {

	@Getter
	@Setter
	private Instant inicioRecoleccion;
	
	@Getter
	@Setter
	private Instant finRecoleccion;
	
	@Getter
	@Setter
	private Integer limite;
	
	@Getter
	@Setter
	private Integer peticiones;
	
	private final DateTimeFormatter formato = DateTimeFormatter.ofPattern( "yyyyMMddHHmm");
	
	public ParametrosRecolector(Integer peticionesRestantes) {
		finRecoleccion = Instant.now();
		inicioRecoleccion = Instant.now();
		peticiones = peticionesRestantes / SimbolosCoinApi.values().length*100;
		for(int contador = 1; contador < peticiones; contador++) {
			inicioRecoleccion = inicioRecoleccion.minus(15, ChronoUnit.MINUTES);
			limite = contador;
		}
	}
	
	public ParametrosRecolector(String ... parametros) throws Exception {
		establecerAtributos(parametros);
		calcularParametrosQueFalten();
		parametros.toString();
	}

	private void calcularParametrosQueFalten() throws Exception {
		if(inicioRecoleccion != null && finRecoleccion != null) {
			Instant tiempoTmp = inicioRecoleccion;
			peticiones = 0;
			while(tiempoTmp.compareTo(finRecoleccion) < 0) {
				tiempoTmp = tiempoTmp.plus(15, ChronoUnit.MINUTES);
				peticiones += 1;
			}
			limite = peticiones;
			peticiones = peticiones * SimbolosCoinApi.values().length / Constantes.COSTE_NUMERO_VELAS_PETICION;
		} else if (inicioRecoleccion != null && finRecoleccion == null && limite != null) {
			Instant tiempoTmp = inicioRecoleccion;
			for (int contador = 1; contador < limite; contador++) {
				tiempoTmp = tiempoTmp.plus(15, ChronoUnit.MINUTES);
			}
			finRecoleccion = tiempoTmp;
			peticiones = limite * SimbolosCoinApi.values().length / Constantes.COSTE_NUMERO_VELAS_PETICION;
		} else if (inicioRecoleccion == null && finRecoleccion != null && limite != null) {
			Instant tiempoTmp = finRecoleccion;
			for (int contador = 1; contador < limite; contador++) {
				tiempoTmp = tiempoTmp.minus(15, ChronoUnit.MINUTES);
			}
			inicioRecoleccion = tiempoTmp;
			peticiones = limite * SimbolosCoinApi.values().length / Constantes.COSTE_NUMERO_VELAS_PETICION;
		}
	}

	private void establecerAtributos(String[] parametros) throws Exception {
		for (String parametro : parametros) {
			parametro = parametro.substring(1);
			String[] parametroTroceado = parametro.split("=");
			if("inicio".equals(parametroTroceado[0])) {
				LocalDateTime ldt = LocalDateTime.parse(parametroTroceado[1] , formato);
				inicioRecoleccion = ldt.toInstant(ZoneOffset.UTC);
			}else if("fin".equals(parametroTroceado[0])) {
				LocalDateTime ldt = LocalDateTime.parse(parametroTroceado[1] , formato);
				finRecoleccion = ldt.toInstant(ZoneOffset.UTC);
			}else if("limite".equals(parametroTroceado[0])) {
				limite = Integer.valueOf(parametroTroceado[1]);
			}
		}
	}
}
