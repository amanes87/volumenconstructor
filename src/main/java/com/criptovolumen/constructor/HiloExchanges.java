package com.criptovolumen.constructor;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.coinapi.entidades.Timedata;
import com.coinapi.enums.Period_identifier;
import com.coinapi.servicios.REST_methods;
import com.criptovolumen.Constantes;
import com.criptovolumen.entidades.vela.par.VelaBTC_15;
import com.criptovolumen.entidades.volumen.par.VolumenBTC;
import com.criptovolumen.entidades.volumen.servicios.VolumenBTCRepository;
import com.criptovolumen.enums.SimbolosCoinApi;
import com.criptovolumen.excepciones.ExcepcionExchange;
import com.criptovolumen.excepciones.RellenarVolumenExcepcion;
import com.criptovolumen.servicios.exchanges.impl.coinbene.CoinbeneSv;
import com.criptovolumen.servicios.temporalidades.TemporalidadesSv;

@Component
public class HiloExchanges {

	Logger logger = LoggerFactory.getLogger(HiloExchanges.class);

	@Autowired
	private ApplicationContext context;

	private int limiteVelas = 96;

	@Autowired
	private VolumenBTCRepository volumenBTCRepository;
	
	@Autowired
	private TemporalidadesSv temporalidadesSv;
	
	//5 minutos = 300000 milisegundos
	private Long tiempoDemora = 300000L;

	@Transactional
	void ejecutar(String... args) {
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		long delay = 0;
		if (args.length > 0) {
			delay = ChronoUnit.MILLIS.between(LocalTime.now(),
					LocalTime.of(Integer.valueOf(args[0]), Integer.valueOf(args[1]), 0));
		}
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					List<VelaBTC_15> velas15minutos = obtenerVolumen15Minutos();
					temporalidadesSv.rellenarVolumen30Minutos(velas15minutos);
					temporalidadesSv.rellenarVolumen60Minutos(velas15minutos);
					temporalidadesSv.rellenarVolumen120Minutos(velas15minutos);
					temporalidadesSv.rellenarVolumen240Minutos(velas15minutos);
					temporalidadesSv.rellenarVolumen720Minutos(velas15minutos);
					temporalidadesSv.rellenarVolumen1Dia(velas15minutos);
					temporalidadesSv.rellenarVolumen1Mes(velas15minutos);
				} catch (Exception e) {
					logger.error("Excepcion durante el proceso de obtener el volumen de 15 minutos");
				}
				logger.info("++++++++++{}++++++++++", LocalTime.now().toString());
			}

		}, delay, 60, TimeUnit.MINUTES);
	}

	/**
	 * Método que obtiene los volumenes de 15 minutos de BTC
	 * @throws ExcepcionExchange 
	 */
	protected List<VelaBTC_15> obtenerVolumen15Minutos() throws ExcepcionExchange {
		List<VelaBTC_15> velas15 = new ArrayList<>();
		REST_methods coinApi = new REST_methods(Constantes.API_KEY_COINAPI);
		boolean hayConexionConServidor = coinApi.conexionConServidor();
		if (hayConexionConServidor) {
			List<VolumenBTC> volumenesBTC = new ArrayList<>();
			Map<Instant, VelaBTC_15> mapVelaBTC = new HashMap<>();
			Map<Instant, Double> mapContadorVolumenes = new HashMap<>();
			mapVelaBTC = obtenerVelasBTC(coinApi, volumenesBTC, mapContadorVolumenes);
			if(!mapVelaBTC.isEmpty()) {
				try {
					volumenesBTC = crearVolumenesConBitfinexBTC_USD(mapVelaBTC, mapContadorVolumenes);
					logger.info("==========={}==============", LocalTime.now().toString());
					if(!volumenesBTC.isEmpty()) {
						obtenerVolumenCoinApi(mapVelaBTC, volumenesBTC, mapContadorVolumenes, coinApi);
						obtenerVolumenCoinBene(mapVelaBTC, volumenesBTC, mapContadorVolumenes);
						contabilizarVolumenYGuardar(volumenesBTC, mapContadorVolumenes);
						velas15 = temporalidadesSv.rellenarVelas15Minutos(mapVelaBTC).stream().sorted((v1,v2) -> v1.getTime_period_start().compareTo(v2.getTime_period_start())).collect(Collectors.toList());
					}
				} catch (Exception e) {
					coinApi.close();
					throw new ExcepcionExchange(context, new Exception("Error durante la construccion del volumenBTC"), e.getMessage());
				}
			}
		} else {
			coinApi.close();
			throw new ExcepcionExchange(context, new Exception("No hay conexión contra COINAPI"));
		}
		coinApi.close();
		return velas15;
	}
	
	/**
	 * Metodo que obtiene las velas de BTC_USD en Bitfinex.
	 * Mientras no obtenga el resultado esperado se embuclará N minutos
	 * @param coinApi
	 * @param volumenesBTC 
	 * @param mapContadorVolumenes 
	 * @return
	 */
	private Map<Instant, VelaBTC_15> obtenerVelasBTC(REST_methods coinApi, List<VolumenBTC> volumenesBTC, Map<Instant, Double> mapContadorVolumenes) {
		Map<Instant, VelaBTC_15> mapVelaBTC = new HashMap<>();
		try {
			// OBTENEMOS LAS VELAS DE BTC_USD EN BITFINEX
			Timedata[] resultadosVelas;
			resultadosVelas = coinApi.ohlcv_get_latest_timeseries(SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getCodigo(), Period_identifier._15MIN, limiteVelas);
			List<VelaBTC_15> velasBTC = Stream.of(resultadosVelas).map(td -> new VelaBTC_15(td))
					.sorted((v1, v2) -> v1.getTime_period_start().compareTo(v2.getTime_period_start()))
					.limit(limiteVelas).distinct().collect(Collectors.toList());
			mapVelaBTC = velasBTC.stream().collect(Collectors.toMap(VelaBTC_15::getTime_period_start, Function.identity()));
		} catch (Exception e) {
			logger.error(e.getMessage());
			new ExcepcionExchange(context, e, "Error al obtener las velas de BTC a las " + LocalDateTime.now());
		}
		return mapVelaBTC;
	}
	
	/**
	 * Método encargado de crear el volumen solo con los datos de las velas obtenidas inicialmente de BitFinex BTC_USD
	 * @param volumenesBTC
	 * @param mapVelaBTC
	 * @param mapContadorVolumenes
	 * @param simboloCoinApi
	 */
	private List<VolumenBTC> crearVolumenesConBitfinexBTC_USD(Map<Instant, VelaBTC_15> mapVelaBTC, Map<Instant, Double> mapContadorVolumenes)
			throws Exception {
		List<VolumenBTC> volumenesBTC = new ArrayList<>();
		for(VelaBTC_15 vela15 : mapVelaBTC.values().stream().sorted((v1, v2) -> v1.getTime_period_start().compareTo(v2.getTime_period_start())).collect(Collectors.toList())){
			VolumenBTC volumenBTC = new VolumenBTC(vela15.getTime_period_start(), vela15.getTime_period_end(), 0);
			PropertyUtils.setProperty(volumenBTC, SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getPropiedadVolumen(), vela15.getVolume_traded());
			PropertyUtils.setProperty(volumenBTC, SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getPropiedadVolumen() + "Min", vela15.getPrice_low());
			PropertyUtils.setProperty(volumenBTC, SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getPropiedadVolumen() + "Max",vela15.getPrice_high());
			double cantidadBitcoin = vela15.getVolume_traded();
			double mediaPrecioUSD = (vela15.getPrice_low()	+ vela15.getPrice_high()) / 2;
			double volumenReal = cantidadBitcoin * mediaPrecioUSD;
			mapContadorVolumenes.put(vela15.getTime_period_start(), volumenReal);
			volumenesBTC.add(volumenBTC);
		};
		return volumenesBTC;
	}

	/**
	 * Metodo que se encarga de obtener los volumenes de los exchanges de CoinAPI
	 * @param mapVelaBTC
	 * @param volumenesBTC
	 * @param mapContadorVolumenes
	 * @param coinApi
	 * @throws Exception 
	 */
	private void obtenerVolumenCoinApi(Map<Instant, VelaBTC_15> mapVelaBTC, List<VolumenBTC> volumenesBTC, Map<Instant, Double> mapContadorVolumenes, REST_methods coinApi) throws Exception {
		for (SimbolosCoinApi simboloCoinApi : Stream.of(SimbolosCoinApi.values()).sorted((sc1, sc2) -> sc1.getCodigo().compareTo(sc2.getCodigo())).filter(s -> s != SimbolosCoinApi.BITFINEX_SPOT_BTC_USD).collect(Collectors.toList())) {
			logger.info("Simbolo: {}", simboloCoinApi.getCodigo());
			try {
				if (!simboloCoinApi.equals(SimbolosCoinApi.BITFINEX_SPOT_BTC_USD)) {
					Timedata[] resultados = coinApi.ohlcv_get_latest_timeseries(simboloCoinApi.getCodigo(), Period_identifier._15MIN, limiteVelas);
					for (Timedata td : resultados) {
						if (mapVelaBTC.containsKey(td.get_time_period_start())) {
							rellenarVolumen(volumenesBTC, td, mapVelaBTC, mapContadorVolumenes, simboloCoinApi);
						}
					}
				}
			} catch (RellenarVolumenExcepcion e) {
				String mensaje = MessageFormat.format("Error durante obtener volumenCoinApi de {0}, Excepcion: {1}", simboloCoinApi.getCodigo(), e.getMessage());
				logger.error(mensaje);
				throw new RellenarVolumenExcepcion(e, mensaje);
			}
		}
	}
	
	/**
	 * Método encargado de rellenar el mapContadorVolumenes con la información recopilada del CoinAPI
	 * @param volumenesBTC
	 * @param td
	 * @param mapVelaBTC
	 * @param mapContadorVolumenes
	 * @param simboloCoinApi
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	private void rellenarVolumen(List<VolumenBTC> volumenesBTC, Timedata td, Map<Instant, VelaBTC_15> mapVelaBTC, Map<Instant, Double> mapContadorVolumenes, SimbolosCoinApi simboloCoinApi)
			throws RellenarVolumenExcepcion {
		Optional<VolumenBTC> opcionalVolumenBTC = Optional.empty();
		try {
		try {
			opcionalVolumenBTC = volumenesBTC.stream()
					.filter(v -> v.getTime_period_start().equals(td.get_time_period_start()) && v.getTime_period_end().equals(td.get_time_period_end()))
					.findFirst();
			
		}catch (NullPointerException e) {
			logger.error("No existe el volumen en el periodo definido en rel Timedata", e.getMessage());
		}
		if (opcionalVolumenBTC.isPresent()) {
			PropertyUtils.setProperty(opcionalVolumenBTC.get(), simboloCoinApi.getPropiedadVolumen(), td.get_volume_traded());
				PropertyUtils.setProperty(opcionalVolumenBTC.get(), simboloCoinApi.getPropiedadVolumen() + "Min", td.get_price_low());
			PropertyUtils.setProperty(opcionalVolumenBTC.get(), simboloCoinApi.getPropiedadVolumen() + "Max", td.get_price_high());
		}
		double cantidadBitcoin = td.get_volume_traded();
		if (simboloCoinApi.getCodigo().endsWith("BTC")) {
			double mediaPrecio = (td.get_price_low() + td.get_price_high()) / 2;
			cantidadBitcoin = cantidadBitcoin * mediaPrecio;
		}
		double mediaPrecioUSD = (mapVelaBTC.get(td.get_time_period_start()).getPrice_low() + mapVelaBTC.get(td.get_time_period_start()).getPrice_high()) / 2;
		double volumenReal = cantidadBitcoin * mediaPrecioUSD;
		mapContadorVolumenes.put(td.get_time_period_start(), mapContadorVolumenes.get(td.get_time_period_start()) + volumenReal);
		} catch (Exception e) {
			logger.error("Error rellenando el volumen {}", e.getMessage());
			throw new RellenarVolumenExcepcion(e);
		}

	}
	
	/**
	 * Método encargado de obtener el volumen de CoinBene BTC_USD
	 * @param mapVelaBTC
	 * @param volumenesBTC
	 * @param mapContadorVolumenes
	 */
	private void obtenerVolumenCoinBene(Map<Instant, VelaBTC_15> mapVelaBTC, List<VolumenBTC> volumenesBTC, Map<Instant, Double> mapContadorVolumenes) {
		//Obtener exchangesFueraDeCoinapi
		Optional<Instant> periodoInicial = Optional.empty();
		Optional<Instant> periodoFinal = Optional.empty();
		try {
			periodoInicial = mapVelaBTC.keySet().stream().sorted().findFirst();
			
		}catch(Exception npe) {
			logger.error("No se ha encontrado periodo inicial en obtenerVolumenCoinBene");
		}
		try {
			periodoFinal = mapVelaBTC.keySet().stream().sorted((v1, v2) -> v2.compareTo(v1)).findFirst();
			
		}catch(Exception npe) {
			logger.error("No se ha encontrado periodo final en obtenerVolumenCoinBene");
		}
		if(periodoInicial.isPresent() && periodoFinal.isPresent()) {
			Map<Instant, Timedata> valoresCoinbene;
			CoinbeneSv coinbeneSv = new CoinbeneSv();
			try {
				valoresCoinbene = coinbeneSv.getOHLC("BTCUSDT", Period_identifier._15MIN, periodoInicial.get(), periodoFinal.get()).stream().collect(Collectors.toMap(Timedata::get_time_period_start, Function.identity()));
				for (VolumenBTC valorVolumenBTC : volumenesBTC) {
					if(valoresCoinbene.containsKey(valorVolumenBTC.getTime_period_start())) {
						Timedata valorCoinBene = valoresCoinbene.get(valorVolumenBTC.getTime_period_start());
						valorVolumenBTC.setCoinbeneSpotBtcUSDT(valorCoinBene.get_volume_traded());
						valorVolumenBTC.setCoinbeneSpotBtcUSDTMin(valorCoinBene.get_price_low());
						valorVolumenBTC.setCoinbeneSpotBtcUSDTMax(valorCoinBene.get_price_high());
						double cantidadBitcoin = valorCoinBene.get_volume_traded();
						double mediaPrecioUSD = (valorCoinBene.get_price_low() + valorCoinBene.get_price_high()) / 2;
						double volumenReal = cantidadBitcoin * mediaPrecioUSD;
						mapContadorVolumenes.put(valorCoinBene.get_time_period_start(), mapContadorVolumenes.get(valorCoinBene.get_time_period_start()) + volumenReal);
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				new ExcepcionExchange(context, e, "Error capturado durante el proceso de obtencion de las velas de coinbene");
			}
			coinbeneSv.close();
		}
	}
	
	/**
	 * Método encargado de hacer la suma del volumen y guardarlo en base de datos solo si es superior a lo que teniamos guardado.
	 * Si fuese inferior querria decir que alguna respuesta del CoinAPI no ha devuelto los datos.
	 * @param volumenesBTC
	 * @param mapContadorVolumenes
	 * @throws ExcepcionExchange 
	 */
	private void contabilizarVolumenYGuardar(List<VolumenBTC> volumenesBTC, Map<Instant, Double> mapContadorVolumenes) throws ExcepcionExchange {
		logger.info("INICIO CONTAJE VOLUMENES");
		try {
			Map<Instant, VolumenBTC> mapVolumenesBTCBaseDeDatos = new HashMap<>();
			volumenBTCRepository.findAll().forEach(vbtc -> {
				if(!mapVolumenesBTCBaseDeDatos.containsKey(vbtc.getTime_period_start())) {
					mapVolumenesBTCBaseDeDatos.put(vbtc.getTime_period_start(), vbtc);
				}
			});
			for (VolumenBTC volumenBTC : volumenesBTC) {
				Double volumenTradeadoTotal = mapContadorVolumenes.get(volumenBTC.getTime_period_start());
				volumenBTC.setVolume_traded(volumenTradeadoTotal == null ? 0 : volumenTradeadoTotal);
				if(!mapVolumenesBTCBaseDeDatos.containsKey(volumenBTC.getTime_period_start())|| mapVolumenesBTCBaseDeDatos.get(volumenBTC.getTime_period_start()).getVolume_traded() <  volumenBTC.getVolume_traded()) {
					volumenBTCRepository.save(volumenBTC);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ExcepcionExchange(context, e, "Error capturado durante el proceso de guardar el volumen en base de datos, despues de hacer las llamadas rest");
		}
	}

	private void dormirHilo(Long tiempoDemoraHilo, String mensajeDemoraError) {
		try {
			Thread.sleep(tiempoDemoraHilo);
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
			new ExcepcionExchange(context, e, mensajeDemoraError);
		}
		
	}

}
