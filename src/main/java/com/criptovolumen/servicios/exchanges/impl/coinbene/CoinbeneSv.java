package com.criptovolumen.servicios.exchanges.impl.coinbene;

import java.io.Closeable;
import java.io.IOException;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.coinapi.entidades.Timedata;
import com.coinapi.enums.Period_identifier;
import com.criptovolumen.servicios.exchanges.ServicioExchange;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class CoinbeneSv implements ServicioExchange, Closeable {

	private final OkHttpClient client = new OkHttpClient();
	
	@Override
	public List<Timedata> getOHLC(String simbolo, Period_identifier periodicidad, Instant momentoInicial, Instant momentoFinal) throws IOException {
		String temporalidad = periodicidad.equals(Period_identifier._15MIN) ? "15" : "15";
		String json = get_json(simbolo, temporalidad, String.valueOf(momentoInicial.getEpochSecond()), String.valueOf(momentoFinal.getEpochSecond()));
		JSONObject resultado = new JSONObject(json);

		JSONArray arrayTiempo = resultado.getJSONArray("t");
		JSONArray arrayApertura = resultado.getJSONArray("o");
		JSONArray arrayCierre = resultado.getJSONArray("c");
		JSONArray arrayMinimo = resultado.getJSONArray("l");
		JSONArray arrayMaximo = resultado.getJSONArray("h");
		JSONArray arrayVolumen = resultado.getJSONArray("v");
		
		List<Timedata> resultados = new ArrayList<>();
		for (int i= 0; i < arrayTiempo.length(); i++) {
			Long tiempo = arrayTiempo.getLong(i);
			double apertura = arrayApertura.getDouble(i);
			double cierre = arrayCierre.getDouble(i);
			double minimo = arrayMinimo.getDouble(i);
			double maximo = arrayMaximo.getDouble(i);
			double volumen = arrayVolumen.getDouble(i);
			Instant periodoInicial = Instant.ofEpochMilli(tiempo * 1000);
			Long milisegundosParaAnyadir = 900000L;
//			if(periodicidad.equals(Period_identifier._15MIN)) {
//				milisegundosParaAnyadir = 900000L;
//			}
			Timedata td = new Timedata(periodoInicial, periodoInicial.plusMillis(milisegundosParaAnyadir), periodoInicial, periodoInicial.plusMillis(milisegundosParaAnyadir), apertura, maximo, minimo, cierre, volumen, 0);
			resultados.add(td);
		}
		return resultados;
	}
	
	private String get_json(String simbolo, String resolution, String instanteInicio, String instanteFin) throws IOException {
		String url = "https://a.coinbene.com/market/tradepair/tradeview/kline/history";
		Request request = new Request.Builder().url(MessageFormat.format(url + "?symbol={0}&resolution={1}&from={2}&to={3}", simbolo, resolution, instanteInicio, instanteFin)).build();

		try (Response response = client.newCall(request).execute() // you MUST always immediately close response,
																	// easiest way is like this; see
																	// https://square.github.io/okhttp/3.x/okhttp/okhttp3/ResponseBody.html
		) {
			ResponseBody body = response.body(); // no need to null check the result of body(): it is annotated
													// @Nullable but its javadoc states that "Returns a non-null value
													// if this response was ... returned from Call.execute()" which is
													// the case above; see
													// https://square.github.io/okhttp/3.x/okhttp/okhttp3/Response.html#body--

			if (response.code() >= 400) {
				String error;
				try {
					JSONObject object = new JSONObject(body.string());
					if (object.has("error")) {
						error = object.getString("error");
					} else {
						error = "[NOTHING: response has no value for \"error\"]";
					}
				} catch (Throwable t) {
					error = "[FAILED to extract response's \"error\" value because this Throwable was raised: " + t
							+ "]";
				}

				String message = "the response code for url is an ERROR code:" + "\n" + "\t" + "url = " + url + "\n"
						+ "\t" + "response code = " + response.code() + "\n" + "\t" + "response body error = " + error
						+ "\n";

				throw new RuntimeException(message);
			}

			return body.string();
		}
	}


	@Override
	public void close() {
		client.dispatcher().executorService().shutdown();
		client.connectionPool().evictAll();
	}
	

}
