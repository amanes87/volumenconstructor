package com.criptovolumen.servicios.exchanges;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import com.coinapi.entidades.Timedata;
import com.coinapi.enums.Period_identifier;

public interface ServicioExchange {

	List<Timedata> getOHLC(String simbolo, Period_identifier periodicidad, Instant momentoInicial, Instant momentoFinal) throws IOException;
}
