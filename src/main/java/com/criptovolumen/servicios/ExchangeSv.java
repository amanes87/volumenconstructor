package com.criptovolumen.servicios;

import java.util.List;

import com.criptovolumen.entidades.volumen.Volumen;

public interface ExchangeSv {

	List<Volumen> getListaVolumenes();
	
}
