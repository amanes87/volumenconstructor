package com.criptovolumen.servicios.telegram;

import java.sql.Timestamp;
import java.text.MessageFormat;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Interfaz con métodos para construir los mensajes que se envian a telegram
 * 
 * @author Adrià
 *
 * @param <Method>
 */
public interface TelegramMensajes<Method> {

	/**
	 * Metodo que envia una notificacion de telegram
	 * @param url
	 * @param map
	 * @return
	 */
	static ResponseEntity<String> enviarNotificacion(String url, MultiValueMap<String, String> map) {
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

		return restTemplate.postForEntity(url, request, String.class);
	}

	/**
	 * Metodo que construye y retorna los parámetros que se se deben enviar en una notificación de telegram
	 * @param chatId
	 * @param cuerpoMensaje
	 * @return
	 */
	static MultiValueMap<String, String> construirParametros(String chatId, String cuerpoMensaje) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("chat_id", chatId);
		map.add("text", cuerpoMensaje);
		map.add("parse_mode", "MARKDOWN");
		return map;
	}

	static String cuerpoMensajeInicio() {
		return "====== INICIO SPRING ======";
	}

	static String cuerpoMensajeFin() {
		return MessageFormat.format("====== APAGADO APLICACION A LAS {0} ======",
				new Timestamp(System.currentTimeMillis()));
	}


	static String cuerpoMensajeNoVenta() {
		// TODO Auto-generated method stub
		return null;
	}

	static String cuerpoMensajeErrorAsync(String message) {
		StringBuilder mensaje = new StringBuilder("EXCEPCION EN HILO {0}");
		return MessageFormat.format(mensaje.toString(), message);
	}

	static String cuerpoMensajeOnError(String message) {
		StringBuilder mensaje = new StringBuilder("EXCEPCION ON ERROR {0}");
		return MessageFormat.format(mensaje.toString(), message);
	}
}
