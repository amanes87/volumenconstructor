package com.criptovolumen.servicios.telegram;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

@Component
public class TelegramRsSv {

	private static final String TEST_CHAT_ID = "-1001214988825";
	private static final String TEST_BASE_URL = "https://api.telegram.org/bot553376516:AAE-4ig3LzcW4Pq-ZIeqCprydXUfYOojIss/sendMessage";

	// private static final String PRODUCCION_CHAT_ID = "-1001349165483";
	// private static final String PRODUCCION_BASE_URL =
	// "https://api.telegram.org/bot603221672:AAGDvhYO76LS-NS2iF4fJbVxtoCAxPNafkI/sendMessage";

	private Logger log = LoggerFactory.getLogger(TelegramRsSv.class);

	/**
	 * Método comun que recibe el cuerpo del mensaje que se quiere enviar por
	 * telegram
	 * 
	 * @param cuerpoMensaje
	 */
	public void notificarComun(String cuerpoMensaje) {
		MultiValueMap<String, String> map = TelegramMensajes.construirParametros(TEST_CHAT_ID, cuerpoMensaje);
		ResponseEntity<String> response = TelegramMensajes.enviarNotificacion(TEST_BASE_URL, map);
		log.info(response.getStatusCode().toString());
	}
	
	/**
	 * Método comun que recibe el cuerpo del mensaje que se quiere enviar por
	 * telegram. Tambien se tiene que informar el chat y la url
	 * 
	 * @param cuerpoMensaje
	 */
	public void notificarCustomChat(String cuerpoMensaje, String ERRORES_CHAT_ID, String ERRORES_BASE_URL) {
		MultiValueMap<String, String> map = TelegramMensajes.construirParametros(ERRORES_CHAT_ID, cuerpoMensaje);
		ResponseEntity<String> response = TelegramMensajes.enviarNotificacion(ERRORES_BASE_URL, map);
		log.info(response.getStatusCode().toString());
	}

}
