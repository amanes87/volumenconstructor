package com.criptovolumen.servicios.temporalidades;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.coinapi.entidades.Timedata;
import com.coinapi.enums.Period_identifier;
import com.coinapi.servicios.REST_methods;
import com.criptovolumen.Constantes;
import com.criptovolumen.constructor.ParametrosRecolector;
import com.criptovolumen.entidades.vela.par.VelaBTC_15;
import com.criptovolumen.entidades.volumen.par.VolumenBTC;
import com.criptovolumen.entidades.volumen.servicios.VolumenBTCRepository;
import com.criptovolumen.enums.SimbolosCoinApi;
import com.criptovolumen.excepciones.ExcepcionExchange;
import com.criptovolumen.excepciones.RellenarVolumenExcepcion;
import com.criptovolumen.servicios.exchanges.impl.coinbene.CoinbeneSv;

@Component
public class RecolectorVelasPasadasSv {

	Logger logger = LoggerFactory.getLogger(RecolectorVelasPasadasSv.class);
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private VolumenBTCRepository volumenBTCRepository;
	
	@Autowired
	private TemporalidadesSv temporalidadesSv;
	
	private ParametrosRecolector parametros;
	
	public void recolectarVelasPasadas(ParametrosRecolector parametros) {
		this.parametros = parametros;
		List<VelaBTC_15> velas15minutos = obtenerVolumen15Minutos();
		velas15minutos = velas15minutos.stream().sorted((v1, v2) -> v1.getTime_period_start().compareTo(v2.getTime_period_start())).collect(Collectors.toList());
		temporalidadesSv.rellenarVolumen30Minutos(velas15minutos);
		temporalidadesSv.rellenarVolumen60Minutos(velas15minutos);
		temporalidadesSv.rellenarVolumen120Minutos(velas15minutos);
		temporalidadesSv.rellenarVolumen240Minutos(velas15minutos);
		temporalidadesSv.rellenarVolumen720Minutos(velas15minutos);
		temporalidadesSv.rellenarVolumen1Dia(velas15minutos);
		temporalidadesSv.rellenarVolumen1Mes(velas15minutos);
	}
	
	private List<VelaBTC_15> obtenerVolumen15Minutos() {
		List<VelaBTC_15> velas15minutos = new ArrayList<>();
		logger.info("Periodo inicial de búsqueda es: {}", parametros.getInicioRecoleccion());
		try {
			REST_methods coinApi = new REST_methods(Constantes.API_KEY_COINAPI);
			boolean hayConexionConServidor = coinApi.conexionConServidor();
			if (hayConexionConServidor) {
				Map<Instant, VelaBTC_15> mapVelaBTC = obtenerVelasBTC(coinApi);
				 Map<Instant, Double> mapContadorVolumenes = new HashMap<>();
				List<VolumenBTC> volumenesBTC = crearVolumenesConBitfinexBTC_USD(mapVelaBTC, mapContadorVolumenes);
				logger.info("==========={}==============", LocalTime.now().toString());
				if(!volumenesBTC.isEmpty()) {
					obtenerVolumenCoinApi(mapVelaBTC, volumenesBTC, mapContadorVolumenes, coinApi);
					obtenerVolumenCoinBene(mapVelaBTC, volumenesBTC, mapContadorVolumenes);
					contabilizarVolumenYGuardar(volumenesBTC, mapContadorVolumenes);
//					velas15minutos = rellenarVelas15Minutos(mapVelaBTC);
					velas15minutos = temporalidadesSv.rellenarVelas15Minutos(mapVelaBTC);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return velas15minutos;
	}
	
	private Map<Instant, VelaBTC_15> obtenerVelasBTC(REST_methods coinApi) throws Exception {
		// OBTENEMOS LAS VELAS DE BTC_USD EN BITFINEX
		Timedata[] resultadosVelas;
			resultadosVelas = coinApi.ohlcv_get_historical_timeseries(SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getCodigo(), Period_identifier._15MIN, parametros.getInicioRecoleccion(), parametros.getLimite());
		List<VelaBTC_15> velasBTC = Stream.of(resultadosVelas)
																.map(td -> new VelaBTC_15(td))
																.sorted((v1, v2) -> v1.getTime_period_start().compareTo(v2.getTime_period_start()))
																.limit(parametros.getLimite()).distinct().collect(Collectors.toList());
		parametros.setInicioRecoleccion(velasBTC.get(0).getTime_period_start());
		parametros.setFinRecoleccion(velasBTC.get(velasBTC.size() -1) .getTime_period_end());
		Map<Instant, VelaBTC_15> mapVelaBTC = new HashMap<>();
		mapVelaBTC = velasBTC.stream().collect(Collectors.toMap(VelaBTC_15::getTime_period_start, Function.identity()));
		return mapVelaBTC;
	}
	
	private List<VolumenBTC> crearVolumenesConBitfinexBTC_USD(Map<Instant, VelaBTC_15> mapVelaBTC, Map<Instant, Double> mapContadorVolumenes) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		List<VolumenBTC> volumenesBTC = new ArrayList<>();
		for(VelaBTC_15 vela15 : mapVelaBTC.values().stream().sorted((v1, v2) -> v1.getTime_period_start().compareTo(v2.getTime_period_start())).collect(Collectors.toList())){
			VolumenBTC volumenBTC = new VolumenBTC(vela15.getTime_period_start(), vela15.getTime_period_end(), 0);
			PropertyUtils.setProperty(volumenBTC, SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getPropiedadVolumen(), vela15.getVolume_traded());
			PropertyUtils.setProperty(volumenBTC, SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getPropiedadVolumen() + "Min", vela15.getPrice_low());
			PropertyUtils.setProperty(volumenBTC, SimbolosCoinApi.BITFINEX_SPOT_BTC_USD.getPropiedadVolumen() + "Max",vela15.getPrice_high());
			double cantidadBitcoin = vela15.getVolume_traded();
			double mediaPrecioUSD = (vela15.getPrice_low()	+ vela15.getPrice_high()) / 2;
			double volumenReal = cantidadBitcoin * mediaPrecioUSD;
			mapContadorVolumenes.put(vela15.getTime_period_start(), volumenReal);
			volumenesBTC.add(volumenBTC);
		};
		return volumenesBTC;
	}

	private void obtenerVolumenCoinApi(Map<Instant, VelaBTC_15> mapVelaBTC, List<VolumenBTC> volumenesBTC, Map<Instant, Double> mapContadorVolumenes, REST_methods coinApi) throws Exception {
		for (SimbolosCoinApi simboloCoinApi : Stream.of(SimbolosCoinApi.values()).sorted((sc1, sc2) -> sc1.getCodigo().compareTo(sc2.getCodigo())).filter(s -> s != SimbolosCoinApi.BITFINEX_SPOT_BTC_USD).collect(Collectors.toList())) {
			logger.info("Simbolo: {}", simboloCoinApi.getCodigo());
			try {
				if (!simboloCoinApi.equals(SimbolosCoinApi.BITFINEX_SPOT_BTC_USD)) {
					Timedata[] resultados = coinApi.ohlcv_get_historical_timeseries(simboloCoinApi.getCodigo(), Period_identifier._15MIN, parametros.getInicioRecoleccion(), parametros.getLimite());
					for (Timedata td : resultados) {
						if (mapVelaBTC.containsKey(td.get_time_period_start())) {
							rellenarVolumen(volumenesBTC, td, mapVelaBTC, mapContadorVolumenes, simboloCoinApi);
						}
					}
				}
			} catch (RellenarVolumenExcepcion e) {
				String mensaje = MessageFormat.format("Error durante obtener volumenCoinApi de {0}, Excepcion: {1}", simboloCoinApi.getCodigo(), e.getMessage());
				logger.error(mensaje);
				throw new RellenarVolumenExcepcion(e, mensaje);
			}
		}
	}
	
	private void rellenarVolumen(List<VolumenBTC> volumenesBTC, Timedata td, Map<Instant, VelaBTC_15> mapVelaBTC, Map<Instant, Double> mapContadorVolumenes, SimbolosCoinApi simboloCoinApi)
			throws RellenarVolumenExcepcion {
		Optional<VolumenBTC> opcionalVolumenBTC = Optional.empty();
		try {
		try {
			opcionalVolumenBTC = volumenesBTC.stream()
					.filter(v -> v.getTime_period_start().equals(td.get_time_period_start()) && v.getTime_period_end().equals(td.get_time_period_end()))
					.findFirst();
			
		}catch (NullPointerException e) {
			logger.error("No existe el volumen en el periodo definido en rel Timedata", e.getMessage());
		}
		if (opcionalVolumenBTC.isPresent()) {
			PropertyUtils.setProperty(opcionalVolumenBTC.get(), simboloCoinApi.getPropiedadVolumen(), td.get_volume_traded());
			PropertyUtils.setProperty(opcionalVolumenBTC.get(), simboloCoinApi.getPropiedadVolumen() + "Min", td.get_price_low());
			PropertyUtils.setProperty(opcionalVolumenBTC.get(), simboloCoinApi.getPropiedadVolumen() + "Max", td.get_price_high());
		}
		double cantidadBitcoin = td.get_volume_traded();
		if (simboloCoinApi.getCodigo().endsWith("BTC")) {
			double mediaPrecio = (td.get_price_low() + td.get_price_high()) / 2;
			cantidadBitcoin = cantidadBitcoin * mediaPrecio;
		}
		double mediaPrecioUSD = (mapVelaBTC.get(td.get_time_period_start()).getPrice_low() + mapVelaBTC.get(td.get_time_period_start()).getPrice_high()) / 2;
		double volumenReal = cantidadBitcoin * mediaPrecioUSD;
		mapContadorVolumenes.put(td.get_time_period_start(), mapContadorVolumenes.get(td.get_time_period_start()) + volumenReal);
		} catch (Exception e) {
			logger.error("Error rellenando el volumen {}", e.getMessage());
			throw new RellenarVolumenExcepcion(e);
		}

	}
	
	private void obtenerVolumenCoinBene(Map<Instant, VelaBTC_15> mapVelaBTC, List<VolumenBTC> volumenesBTC, Map<Instant, Double> mapContadorVolumenes) {
		//Obtener exchangesFueraDeCoinapi
		Optional<Instant> periodoInicial = Optional.empty();
		Optional<Instant> periodoFinal = Optional.empty();
		try {
			periodoInicial = mapVelaBTC.keySet().stream().sorted().findFirst();
			
		}catch(Exception npe) {
			logger.error("No se ha encontrado periodo inicial en obtenerVolumenCoinBene");
		}
		try {
			periodoFinal = mapVelaBTC.keySet().stream().sorted((v1, v2) -> v2.compareTo(v1)).findFirst();
			
		}catch(Exception npe) {
			logger.error("No se ha encontrado periodo final en obtenerVolumenCoinBene");
		}
		if(periodoInicial.isPresent() && periodoFinal.isPresent()) {
			Map<Instant, Timedata> valoresCoinbene;
			CoinbeneSv coinbeneSv = new CoinbeneSv();
			try {
				valoresCoinbene = coinbeneSv.getOHLC("BTCUSDT", Period_identifier._15MIN, periodoInicial.get(), periodoFinal.get()).stream().collect(Collectors.toMap(Timedata::get_time_period_start, Function.identity()));
				for (VolumenBTC valorVolumenBTC : volumenesBTC) {
					if(valoresCoinbene.containsKey(valorVolumenBTC.getTime_period_start())) {
						Timedata valorCoinBene = valoresCoinbene.get(valorVolumenBTC.getTime_period_start());
						valorVolumenBTC.setCoinbeneSpotBtcUSDT(valorCoinBene.get_volume_traded());
						valorVolumenBTC.setCoinbeneSpotBtcUSDTMin(valorCoinBene.get_price_low());
						valorVolumenBTC.setCoinbeneSpotBtcUSDTMax(valorCoinBene.get_price_high());
						double cantidadBitcoin = valorCoinBene.get_volume_traded();
						double mediaPrecioUSD = (valorCoinBene.get_price_low() + valorCoinBene.get_price_high()) / 2;
						double volumenReal = cantidadBitcoin * mediaPrecioUSD;
						mapContadorVolumenes.put(valorCoinBene.get_time_period_start(), mapContadorVolumenes.get(valorCoinBene.get_time_period_start()) + volumenReal);
					}
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				new ExcepcionExchange(context, e, "Error capturado durante el proceso de obtencion de las velas de coinbene");
			}
			coinbeneSv.close();
		}
	}
	
	private void contabilizarVolumenYGuardar(List<VolumenBTC> volumenesBTC, Map<Instant, Double> mapContadorVolumenes) throws ExcepcionExchange {
		logger.info("INICIO CONTAJE VOLUMENES");
		try {
			Map<Instant, VolumenBTC> mapVolumenesBTCBaseDeDatos = new HashMap<>();
			volumenBTCRepository.findAll().forEach(vbtc -> {
				if(!mapVolumenesBTCBaseDeDatos.containsKey(vbtc.getTime_period_start())) {
					mapVolumenesBTCBaseDeDatos.put(vbtc.getTime_period_start(), vbtc);
				}
			});
			for (VolumenBTC volumenBTC : volumenesBTC) {
				Double volumenTradeadoTotal = mapContadorVolumenes.get(volumenBTC.getTime_period_start());
				volumenBTC.setVolume_traded(volumenTradeadoTotal == null ? 0 : volumenTradeadoTotal);
				if(!mapVolumenesBTCBaseDeDatos.containsKey(volumenBTC.getTime_period_start())|| mapVolumenesBTCBaseDeDatos.get(volumenBTC.getTime_period_start()).getVolume_traded() <  volumenBTC.getVolume_traded()) {
					volumenBTCRepository.save(volumenBTC);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ExcepcionExchange(context, e, "Error capturado durante el proceso de guardar el volumen en base de datos, despues de hacer las llamadas rest");
		}
	}

}
