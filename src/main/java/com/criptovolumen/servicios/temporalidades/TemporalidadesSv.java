package com.criptovolumen.servicios.temporalidades;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.criptovolumen.entidades.vela.par.VelaBTC_120;
import com.criptovolumen.entidades.vela.par.VelaBTC_15;
import com.criptovolumen.entidades.vela.par.VelaBTC_240;
import com.criptovolumen.entidades.vela.par.VelaBTC_30;
import com.criptovolumen.entidades.vela.par.VelaBTC_60;
import com.criptovolumen.entidades.vela.par.VelaBTC_720;
import com.criptovolumen.entidades.vela.par.VelaBTC_D;
import com.criptovolumen.entidades.vela.par.VelaBTC_M;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_120Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_15Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_240Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_30Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_60Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_720Repository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_DRepository;
import com.criptovolumen.entidades.vela.servicios.VelaBTC_MRepository;
import com.criptovolumen.entidades.volumen.par.VolumenBTC;
import com.criptovolumen.entidades.volumen.servicios.VolumenBTCRepository;

@Component
public class TemporalidadesSv {
	
	Logger logger = LoggerFactory.getLogger(TemporalidadesSv.class);

	@Autowired
	private VolumenBTCRepository volumenBTCRepository;
	
	@Autowired
	private VelaBTC_15Repository velaBTC15Repository;
	
	@Autowired
	private VelaBTC_30Repository velaBTC30Repository;
	
	@Autowired
	private VelaBTC_60Repository velaBTC60Repository;
	
	@Autowired
	private VelaBTC_120Repository velaBTC120Repository;
	
	@Autowired
	private VelaBTC_240Repository velaBTC240Repository;
	
	@Autowired
	private VelaBTC_720Repository velaBTC720Repository;
	
	@Autowired
	private VelaBTC_DRepository velaBTCDRepository;
	
	@Autowired
	private VelaBTC_MRepository velaBTCMRepository;
	
	@Transactional
	public List<VelaBTC_15> rellenarVelas15Minutos(Map<Instant, VelaBTC_15> mapVelaBTC) {
		List<VelaBTC_15> velas15minutos = new ArrayList<>();
		try {
			logger.info("Rellenando velas 15Minutos");
			Optional<Instant> instanteInicial = Optional.empty();
			Optional<Instant> instanteFinal = Optional.empty();
			try {
				instanteInicial = mapVelaBTC.keySet().stream().min((v1, v2) -> v1.compareTo(v2));
			}catch (NullPointerException npe) {
				logger.error("No se ha encontrado instante inicial en velas15minutos");
			}
			try {
				instanteFinal = mapVelaBTC.keySet().stream().max((v1, v2) -> v1.compareTo(v2));
			}catch (NullPointerException npe) {
				logger.error("No se ha encontrado instante final en velas15minutos");
			}
			
			if(instanteInicial.isPresent() && instanteFinal.isPresent()) {
				Map<Instant, VolumenBTC> mapVolumenBTC = volumenBTCRepository.findAllSimpleVolumenBTCBetweenDates(instanteInicial.get(), instanteFinal.get()).stream().collect(Collectors.toMap(VolumenBTC::getTime_period_start, Function.identity()));
				mapVelaBTC.entrySet().forEach(es ->{
					if(mapVolumenBTC.containsKey(es.getKey())) {
						es.getValue().setVolume_traded(mapVolumenBTC.get(es.getKey()).getVolume_traded());
						velas15minutos.add(velaBTC15Repository.save(es.getValue()));
					}
				});
			}
		}catch(Exception e) {
			logger.error("Excepcion durante el proceso de rellenar el volumen15minutos {}", e.getMessage());
		}
		return velas15minutos;
	}

	@Transactional
	public List<VelaBTC_30> rellenarVolumen30Minutos(List<VelaBTC_15> velas15minutos ) {
		logger.info("Rellenando velas 30Minutos");
		List<VelaBTC_30> velas30minutos = new ArrayList<>();
		VelaBTC_30 vela30Minutos = null;
		for (VelaBTC_15 vela : velas15minutos) {
			if(vela30Minutos == null && (vela.getTime_period_start().toString().endsWith("00:00Z") || vela.getTime_period_start().toString().endsWith("30:00Z"))) {
				vela30Minutos = new VelaBTC_30(vela);
			}
			if(vela30Minutos != null && (vela.getTime_period_start().toString().endsWith("15:00Z") || vela.getTime_period_start().toString().endsWith("45:00Z"))) {
				vela30Minutos.cerrarVela(vela);
				velas30minutos.add(vela30Minutos);
				vela30Minutos = null;
			}
		}
		if(vela30Minutos != null) {
			velas30minutos.add(vela30Minutos);
		}
		velaBTC30Repository.saveAll(velas30minutos);
		return velas30minutos;
	}

	@Transactional
	public List<VelaBTC_60> rellenarVolumen60Minutos(List<VelaBTC_15> velas15minutos) {
		logger.info("Rellenando velas 60Minutos");
		List<VelaBTC_60> velas60minutos = new ArrayList<>();
		VelaBTC_60 vela60Minutos = null;
		for (VelaBTC_15 vela : velas15minutos) {
			if(vela60Minutos == null && vela.getTime_period_start().toString().endsWith("00:00Z")) {
				vela60Minutos = new VelaBTC_60(vela);
			}else if(vela60Minutos != null && vela.getTime_period_end().toString().endsWith("00:00Z")) {
				vela60Minutos.cerrarVela(vela);
				velas60minutos.add(vela60Minutos);
				vela60Minutos = null;
			}else if(vela60Minutos != null && !vela.getTime_period_start().toString().endsWith("00:00Z") && !vela.getTime_period_end().toString().endsWith("00:00Z")) {
				vela60Minutos.cerrarVela(vela);
			}
		}
		if(vela60Minutos != null) {
			velas60minutos.add(vela60Minutos);
		}
		velaBTC60Repository.saveAll(velas60minutos);
		return velas60minutos;
	}

	@Transactional
	public List<VelaBTC_120> rellenarVolumen120Minutos(List<VelaBTC_15> velas15minutos) {
		logger.info("Rellenando velas 120Minutos");
		List<VelaBTC_120> velas120minutos = new ArrayList<>();
		VelaBTC_120 vela120Minutos = null;
		Set<String> numeroPar = Arrays.asList("0", "2", "4", "6", "8").stream().collect(Collectors.toSet());
		for (VelaBTC_15 vela : velas15minutos) {
			String[] arrayHora = vela.getTime_period_start().toString().split(":");
			if(vela120Minutos == null && numeroPar.contains(String.valueOf(arrayHora[0].charAt(arrayHora[0].length() - 1))) && vela.getTime_period_start().toString().endsWith("00:00Z")) {
				vela120Minutos = new VelaBTC_120(vela);
			}else if(vela120Minutos != null &&  vela.getTimePeriodEndLocalDateTime().equals(vela120Minutos.getTimePeriodStartLocalDateTime().plusHours(2))) {
				vela120Minutos.cerrarVela(vela);
				velas120minutos.add(vela120Minutos);
				vela120Minutos = null;
			} else if(vela120Minutos != null && vela.getTimePeriodEndLocalDateTime().isBefore(vela120Minutos.getTimePeriodEndLocalDateTime().plusHours(2))) {
				vela120Minutos.cerrarVela(vela);
			}
		}
		if(vela120Minutos != null) {
			velas120minutos.add(vela120Minutos);
		}
		velaBTC120Repository.saveAll(velas120minutos);
		return velas120minutos;
	}

	@Transactional
	public List<VelaBTC_240> rellenarVolumen240Minutos(List<VelaBTC_15> velas15minutos) {
		logger.info("Rellenando velas 240Minutos");
		List<VelaBTC_240> velas240minutos = new ArrayList<>();
		VelaBTC_240 vela240Minutos = null;
		String[] numeroInicio = {"00", "04", "08", "12", "16", "20"};
		for (VelaBTC_15 vela : velas15minutos) {
			String[] arrayHoraInicio = vela.getTime_period_start().toString().split(":");
			if(vela240Minutos == null && StringUtils.endsWithAny(arrayHoraInicio[0], numeroInicio) && vela.getTime_period_start().toString().endsWith("00:00Z")) {
				vela240Minutos = new VelaBTC_240(vela);
			}else if(vela240Minutos != null && vela.getTimePeriodEndLocalDateTime().equals(vela240Minutos.getTimePeriodStartLocalDateTime().plusHours(4))) {
				vela240Minutos.cerrarVela(vela);
				velas240minutos.add(vela240Minutos);
				vela240Minutos = null;
			} else if(vela240Minutos != null && vela.getTimePeriodEndLocalDateTime().isBefore(vela240Minutos.getTimePeriodEndLocalDateTime().plusHours(4))) {
				vela240Minutos.cerrarVela(vela);
			}
		}
		if(vela240Minutos != null) {
			velas240minutos.add(vela240Minutos);
		}
		velaBTC240Repository.saveAll(velas240minutos);
		return velas240minutos;
	}
	
	@Transactional
	public List<VelaBTC_720> rellenarVolumen720Minutos(List<VelaBTC_15> velas15minutos) {
		logger.info("Rellenando velas 720Minutos");
		List<VelaBTC_720> velas720minutos = new ArrayList<>();
		VelaBTC_720 vela720Minutos = null;
		String[] numeroInicio = {"00", "12"};
		for (VelaBTC_15 vela : velas15minutos) {
			String[] arrayHoraInicio = vela.getTime_period_start().toString().split(":");
			if(vela720Minutos == null && StringUtils.endsWithAny(arrayHoraInicio[0], numeroInicio) && vela.getTime_period_start().toString().endsWith("00:00Z")) {
				vela720Minutos = new VelaBTC_720(vela);
			}else if(vela720Minutos != null && vela.getTimePeriodEndLocalDateTime().equals(vela720Minutos.getTimePeriodStartLocalDateTime().plusHours(12))) {
				vela720Minutos.cerrarVela(vela);
				velas720minutos.add(vela720Minutos);
				vela720Minutos = null;
			} else if(vela720Minutos != null && vela.getTimePeriodEndLocalDateTime().isBefore(vela720Minutos.getTimePeriodEndLocalDateTime().plusHours(12))){
				vela720Minutos.cerrarVela(vela);
			}
		}
		if(vela720Minutos != null) {
			velas720minutos.add(vela720Minutos);
		}
		velaBTC720Repository.saveAll(velas720minutos);
		return velas720minutos;
	}
	
	@Transactional
	public List<VelaBTC_D> rellenarVolumen1Dia(List<VelaBTC_15> velas15minutos) {
		logger.info("Rellenando velas 1D");
		List<VelaBTC_D> velas1Dia = new ArrayList<>();
		VelaBTC_D vela1Dia = null;
		String[] numeroInicio = {"00"};
		for (VelaBTC_15 vela : velas15minutos) {
			String[] arrayHoraInicio = vela.getTime_period_start().toString().split(":");
			if(vela1Dia == null && StringUtils.endsWithAny(arrayHoraInicio[0], numeroInicio)) {
				vela1Dia = new VelaBTC_D(vela);
			} else  if(vela1Dia != null && vela.getTimePeriodEndLocalDateTime().equals(vela1Dia.getTimePeriodStartLocalDateTime().plusHours(24))) {
				vela1Dia.cerrarVela(vela);
				velas1Dia.add(vela1Dia);
				vela1Dia = null;
			} else if(vela1Dia != null && vela.getTimePeriodEndLocalDateTime().isBefore(vela1Dia.getTimePeriodEndLocalDateTime().plusHours(24))) {
				vela1Dia.cerrarVela(vela);
			}
		}
		if(vela1Dia != null) {
			velas1Dia.add(vela1Dia);
		}
		velaBTCDRepository.saveAll(velas1Dia);
		return velas1Dia;
	}
	
	@Transactional
	public List<VelaBTC_M> rellenarVolumen1Mes(List<VelaBTC_15> velas15minutos) {
		logger.info("Rellenando velas 1M");
		List<VelaBTC_M> velas1Mes = new ArrayList<>();
		VelaBTC_M vela1Mes = null;
		String formatoFechaInicioMes = "01T00:00:00Z";
		for (VelaBTC_15 vela : velas15minutos) {
			if(vela1Mes == null && vela.getTime_period_start().toString().endsWith(formatoFechaInicioMes)) {
				vela1Mes = new VelaBTC_M(vela);
			} else  if(vela1Mes != null && vela.getTimePeriodEndLocalDateTime().equals(vela1Mes.getTimePeriodStartLocalDateTime().plusMonths(1))) {
				vela1Mes.cerrarVela(vela);
				velas1Mes.add(vela1Mes);
				vela1Mes = null;
			} else if(vela1Mes != null &&  vela.getTimePeriodEndLocalDateTime().isBefore(vela1Mes.getTimePeriodEndLocalDateTime().plusMonths(1))) {
				vela1Mes.cerrarVela(vela);
			}
		}
		if(vela1Mes != null) {
			velas1Mes.add(vela1Mes);
		}
		velaBTCMRepository.saveAll(velas1Mes);
		return velas1Mes;
	}
	
}
