package com.criptovolumen.excepciones;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import com.criptovolumen.servicios.telegram.TelegramRsSv;

public class ExcepcionExchange extends Exception {
 
	private static final long serialVersionUID = -7239036738983511017L;
	
	private static final String ERRORES_CHAT_ID = "-1001382156898";
	private static final String ERRORES_BASE_URL = "https://api.telegram.org/bot623771397:AAGKPnBwlNlHcjUvB5JxLxcWiDwH5REcct8/sendMessage";

	public ExcepcionExchange (ApplicationContext applicationContext, Exception e) {
		tratarExcepcionComun(applicationContext, e);
	}
	
	public ExcepcionExchange (ApplicationContext applicationContext, Exception e, String preMensaje) {
		tratarExcepcionComun(applicationContext, new Exception(preMensaje + " " + e.getMessage()));
	}
	
	public ExcepcionExchange(ApplicationContext applicationContext, String mensaje) {
		tratarExcepcionComun(applicationContext, new Exception(mensaje));
	}

	private void tratarExcepcionComun(ApplicationContext applicationContext, Exception e) {
		TelegramRsSv telegramSv = new TelegramRsSv();
		AutowireCapableBeanFactory factory = applicationContext.getAutowireCapableBeanFactory();
		factory.autowireBean( telegramSv );
		factory.initializeBean( telegramSv, "telegramSv" );
		telegramSv.notificarCustomChat(e.getMessage(), ERRORES_CHAT_ID, ERRORES_BASE_URL);
		
	}
}
