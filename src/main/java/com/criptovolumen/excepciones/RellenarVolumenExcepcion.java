package com.criptovolumen.excepciones;

public class RellenarVolumenExcepcion extends Exception {


	private static final long serialVersionUID = -819391347756501486L;

	public RellenarVolumenExcepcion(Exception e) {
		super(e);
	}

	public RellenarVolumenExcepcion(RellenarVolumenExcepcion e, String mensaje) {
		super(mensaje, e);
	}
}
