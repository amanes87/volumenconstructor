package com.criptovolumen.constructor;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@Deprecated
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ApplicationContext.class })
@EnableConfigurationProperties
public class ConstructorApplicationTests {

	Logger logger = LoggerFactory.getLogger(ConstructorApplicationTests.class);

	@Autowired
	private ApplicationContext context;

	@Test
	public void testConversionTimestampAMiliseconds() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'hh:mm:ss.SSS'Z'");
		String dateInString = "22-01-2015T10:20:56.000Z";
		Date date = sdf.parse(dateInString);
		logger.info(dateInString);
		logger.info("Date - Time in milliseconds : {}", date.getTime());
		assertEquals(1421918456000L, date.getTime());
	}


//	String[] MEJORES_MARKETCAP = { "BTC", "ETH", "XRP", "BCH", "EOS", "XLM", "LTC", "USDT", "ADA", "XMR", "TRX", "IOTA",
//			"DASH", "BNB", "NEO", "ETC", "XEM", "XTZ", "VET", "DOGE", "ZEC", "OMG", "MKR", "BTG", "ONT", "ZRX", "LSK",
//			"QTUM", "DCR", "BCN" };

//	@Test
//	public void construirEnumSimbolosCoinmarket() {
//		try {
//			List<String> lines = FileUtils.readLines(new File("C:\\temp\\TODOS_LOS_SYMBOLS_COINAPI.txt"), "UTF-8");
//			List<String> lineasResultantes = new ArrayList<>();
//			lineasResultantes.add("package com.criptovolumen.entidades.par;");
//			lineasResultantes.add("");
//			lineasResultantes.add("import lombok.Getter;");
//			lineasResultantes.add("");
//			lineasResultantes.add("public enum TipoExchangeParUSD {");
//			lineasResultantes.add("");
//			Set<String> mapMejoresMarketCap = Stream.of(MEJORES_MARKETCAP).collect(Collectors.toSet());
//			for (String line : lines) {
//				if (line.contains("symbol_id")) {
//					String linea = line.split(":")[1];
//					String simbolo = StringUtils.substringBetween(linea, "\"");
//					if (!simbolo.matches(".*\\d+.*")
//							&& mapMejoresMarketCap.contains(
//									StringUtils.substringBefore(StringUtils.substringAfter(simbolo, "SPOT_"), "_"))
//							&& simbolo.endsWith("USD")) {
//						String resultado = simbolo.concat("(\"" + simbolo + "\"),//");
//						lineasResultantes.add(resultado);
//					}
//				}
//			}
//			lineasResultantes.add("");
//			lineasResultantes.add("@Getter");
//			lineasResultantes.add("private String codigo;");
//			lineasResultantes.add("");
//			lineasResultantes.add("private TipoExchangeParUSD(String codigo) {");
//			lineasResultantes.add("this.codigo = codigo;");
//			lineasResultantes.add("}");
//			lineasResultantes.add("}");
//			FileUtils.writeLines(new File("C:\\temp\\TODOS_LOS_SYMBOLS_COINAPI_RESULTADO.txt"), lineasResultantes);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		assertEquals(0, 0);
//	}

//	@Test
//	public void construirEntidadesVolumen() {
//		List<String> exchanges = Stream.of(TipoExchangeParUSD.values())
//				.map(simbolo -> StringUtils.substringBefore(simbolo.getCodigo(), "_SPOT").toLowerCase()).distinct()
//				.sorted().collect(Collectors.toList());
//		for (String simbolo : MEJORES_MARKETCAP) {
//			String nombreClase = "Volumen" + StringUtils.capitalize(simbolo.toLowerCase()) + "Usd";
//			String nombreArchivo = nombreClase + ".java";
//
//			List<String> lineasEscribir = new ArrayList<>();
//			lineasEscribir.add("package com.criptovolumen.entidades.volumen.par;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import javax.persistence.Table;");
//			lineasEscribir.add("import javax.persistence.Entity;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import com.criptovolumen.entidades.volumen.Volumen;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import lombok.Getter;");
//			lineasEscribir.add("import lombok.Setter;");
//			lineasEscribir.add("");
//			lineasEscribir.add("@Getter");
//			lineasEscribir.add("@Setter");
//			lineasEscribir.add("@Entity");
//			lineasEscribir.add("@Table(name = \"VOLUMEN_" + simbolo + "_USD\")");
//			lineasEscribir.add("public class " + nombreClase + " extends Volumen {");
//			lineasEscribir.add("");
//			for (String exchange : exchanges) {
//				lineasEscribir.add("");
//				lineasEscribir.add("private double " + exchange + ";");
//				lineasEscribir.add("");
//			}
//			lineasEscribir.add("}");
//			try {
//				FileUtils.writeLines(new File("C:\\temp\\VOLUMEN\\" + nombreArchivo), lineasEscribir);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		assertEquals(0, 0);
//
//	}

//	@Test
//	public void construirEntidadesVela() {
//		for (String simbolo : MEJORES_MARKETCAP) {
//			String nombreClase = "Vela" + StringUtils.capitalize(simbolo.toLowerCase()) + "Usd";
//			String nombreArchivo = nombreClase + ".java";
//
//			List<String> lineasEscribir = new ArrayList<>();
//			lineasEscribir.add("package com.criptovolumen.entidades.vela.par;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import javax.persistence.Table;");
//			lineasEscribir.add("import javax.persistence.Entity;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import com.criptovolumen.entidades.vela.Vela;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import lombok.Getter;");
//			lineasEscribir.add("import lombok.Setter;");
//			lineasEscribir.add("");
//			lineasEscribir.add("@Getter");
//			lineasEscribir.add("@Setter");
//			lineasEscribir.add("@Entity");
//			lineasEscribir.add("@Table(name = \"VELA_" + simbolo + "_USD\")");
//			lineasEscribir.add("public class " + nombreClase + " extends Vela {");
//			lineasEscribir.add("");
//			lineasEscribir.add("}");
//			try {
//				FileUtils.writeLines(new File("C:\\temp\\VELA\\" + nombreArchivo), lineasEscribir);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}
//		assertEquals(0, 0);
//
//	}
	
//	@Test
//	public void construirServiciosVela() {
//		for (String simbolo : MEJORES_MARKETCAP) {
//			String nombreEntidad = "Vela" + StringUtils.capitalize(simbolo.toLowerCase()) + "Usd";
//			String nombreInterfaz = nombreEntidad + "Repository";
//			String nombreArchivo = nombreInterfaz + ".java";
//
//			List<String> lineasEscribir = new ArrayList<>();
//			lineasEscribir.add("package com.criptovolumen.entidades.vela.servicios;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import org.springframework.data.repository.CrudRepository;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import com.criptovolumen.entidades.vela.par." + nombreEntidad + ";");
//			lineasEscribir.add("");
//			lineasEscribir.add("public interface " + nombreInterfaz + " extends CrudRepository<"+ nombreEntidad + ", Long>{");
//			lineasEscribir.add("");
//			lineasEscribir.add("}");
//			try {
//				FileUtils.writeLines(new File("C:\\temp\\VELA_SERVICIOS\\" + nombreArchivo), lineasEscribir);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}
//		assertEquals(0, 0);
//
//	}
	
//	@Test
//	public void construirServiciosVolumen() {
//		for (String simbolo : MEJORES_MARKETCAP) {
//			String nombreEntidad = "Volumen" + StringUtils.capitalize(simbolo.toLowerCase()) + "Usd";
//			String nombreInterfaz = nombreEntidad + "Repository";
//			String nombreArchivo = nombreInterfaz + ".java";
//
//			List<String> lineasEscribir = new ArrayList<>();
//			lineasEscribir.add("package com.criptovolumen.entidades.volumen.servicios;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import org.springframework.data.repository.CrudRepository;");
//			lineasEscribir.add("");
//			lineasEscribir.add("import com.criptovolumen.entidades.volumen.par." + nombreEntidad + ";");
//			lineasEscribir.add("");
//			lineasEscribir.add("public interface " + nombreInterfaz + " extends CrudRepository<"+ nombreEntidad + ", Long>{");
//			lineasEscribir.add("");
//			lineasEscribir.add("}");
//			try {
//				FileUtils.writeLines(new File("C:\\temp\\VOLUMEN_SERVICIOS\\" + nombreArchivo), lineasEscribir);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}
//		assertEquals(0, 0);
//
//	}

}
