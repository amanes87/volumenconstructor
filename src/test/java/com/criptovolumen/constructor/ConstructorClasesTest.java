package com.criptovolumen.constructor;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.junit4.SpringRunner;

import com.coinapi.entidades.Timedata;
import com.criptovolumen.enums.SimbolosCoinApi;

import lombok.NoArgsConstructor;


@RunWith(SpringRunner.class)
@EnableConfigurationProperties
public class ConstructorClasesTest {
	
	Logger logger = LoggerFactory.getLogger(ConstructorClasesTest.class);

//	MONEDAS ORIGINALES
//	private String[] monedasFiat = {"AED","AFN","ALL","AMD","AOA","ARS","AUD","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BND","BOB","BRL","BSD","BTN","BWP","BYR","BZD","CAD","CDF","CHF","CLP","CNY","COP","CRC","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","EUR","FJD","FKP","GBP","GEL","GHS","GIP","GMD","GNF","GTQ","GYD","HKD","HNL","HRK","HTG","HUF","IDR","ILS","INR","IQD","IRR","ISK","JMD","JOD","JPY","KES","KGS","KHR","KPW","KRW","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MUR","MVR","MWK","MXN","MYR","MZN","NAD","NGN","NIO","NOK","NPR","NZD","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","RUB","RWF","SAR","SBD","SCR","SDG","SEK","SGD","SHP","SLL","SOS","SRD","STD","SYP","SZL","THB","TJS","TMT","TND","TOP","TRY","TTD","TWD","TZS","UAH","UGX","USD","UYU","UZS","VEF","VND","VUV","WST","XAF","XCD","XPF","YER","ZAR","ZMW","ZWL"};
	private String[] monedasFiat = {"AED","AFN","AMD","ARS","AUD","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BRL","BWP","BYR","BZD","CAD","CDF","CHF","CLP","CNY","COP","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","EUR","FJD","FKP","GBP","GEL","GHS","GIP","GMD","GNF","GTQ","GYD","HKD","HNL","HRK","HTG","HUF","IDR","ILS","INR","IQD","IRR","ISK","JMD","JOD","JPY","KES","KGS","KHR","KPW","KRW","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MUR","MVR","MWK","MXN","MYR","MZN","NAD","NGN","NOK","NPR","NZD","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","RUB","RWF","SAR","SDG","SEK","SGD","SLL","SOS","SRD","STD","SYP","SZL","THB","TJS","TND","TOP","TRY","TTD","TWD","TZS","UAH","UGX","USD", "USDT", "TUSD","UYU","UZS","VEF","VND","VUV","WST","XAF","XPF","YER","ZAR","ZMW","ZWL"};
	
	/**
	 * Método encargado de construir la clase SimbolosCoinApi. Basadas en BTC
	 */
	@Test
	public void construirSimbolosCoinApiQueUtilizenMonedasCompradorasParaBTC() {
		try {
			List<String> lines = FileUtils.readLines(new File("C:\\temp\\TODOS_LOS_SYMBOLS_COINAPI.txt"), "UTF-8");
			List<String> lineasResultantes = new ArrayList<>();
			lineasResultantes.add("package com.criptovolumen.enums;");
			lineasResultantes.add("");
			lineasResultantes.add("import lombok.Getter;");
			lineasResultantes.add("");
			lineasResultantes.add("public enum SimbolosCoinApi {");
			lineasResultantes.add("");
			for (String line : lines) {
				if (line.contains("symbol_id")) {
					String linea = line.split(":")[1];
					String simbolo = StringUtils.substringBetween(linea, "\"");
					for(String simboloFiat : monedasFiat) {
						String condicionUno = "_BTC_"+simboloFiat;
						String condicionDos = "_"+ simboloFiat + "_BTC";
						if (!simbolo.matches(".*\\d+.*") //
								&& !linea.contains("@")
								&& (simbolo.endsWith(condicionUno) || simbolo.endsWith(condicionDos))
								&& !simbolo.contains("PERP")
								&& !simbolo.equals("BITMEX_SPOT_BTC_USD")) { //
							String[] partes = StringUtils.split(simbolo, "_");
							String propiedadVolumen = StringUtils.uncapitalize(Arrays.asList(partes).stream().map(p -> StringUtils.capitalize(p.toLowerCase())).collect(Collectors.joining()));
							String resultado = simbolo.concat("(\"" + simbolo + "\", \"" + propiedadVolumen + "\"),//");
							lineasResultantes.add(resultado);
							break;
						}
					}
				}
			}
			lineasResultantes.add("");
			lineasResultantes.add("@Getter");
			lineasResultantes.add("private String codigo;");
			lineasResultantes.add("");
			lineasResultantes.add("@Getter");
			lineasResultantes.add("private String propiedadVolumen;");
			lineasResultantes.add("");
			lineasResultantes.add("private SimbolosCoinApi(String codigo, String propiedadVolumen) {");
			lineasResultantes.add("this.codigo = codigo;");
			lineasResultantes.add("this.propiedadVolumen = propiedadVolumen;");
			lineasResultantes.add("}");
			lineasResultantes.add("}");
			FileUtils.writeLines(new File("C:\\temp\\TODOS_LOS_SYMBOLS_COINAPI_RESULTADO.txt"), lineasResultantes);
			assertEquals(0, 0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertEquals(0, 1);
		}
	}
	
	/**
	 * Método encargado de construir la entidad de volumen
	 */
	@Test
	public void construirEntidadVolumen() {
			String nombreClase = "VolumenBTC";
			String nombreArchivo = nombreClase + ".java";

			List<String> lineasEscribir = new ArrayList<>();
			lineasEscribir.add("package com.criptovolumen.entidades.volumen.par;");
			lineasEscribir.add("");
			lineasEscribir.add("import javax.persistence.Table;");
			lineasEscribir.add("import javax.persistence.Entity;");
			lineasEscribir.add("");
			lineasEscribir.add("import com.criptovolumen.entidades.volumen.Volumen;");
			lineasEscribir.add("");
			lineasEscribir.add("import lombok.Getter;");
			lineasEscribir.add("import lombok.Setter;");
			lineasEscribir.add("import lombok.NoArgsConstructor;");
			lineasEscribir.add("import com.coinapi.entidades.Timedata;");
			lineasEscribir.add("");
			lineasEscribir.add("@Getter");
			lineasEscribir.add("@Setter");
			lineasEscribir.add("@Entity");
			lineasEscribir.add("@NoArgsConstructor");
			lineasEscribir.add("@Table(name = \"VOLUMEN_BTC\")");
			lineasEscribir.add("public class " + nombreClase + " extends Volumen {");
			lineasEscribir.add("");
			for (SimbolosCoinApi simboloCoinApi : SimbolosCoinApi.values()) {
				lineasEscribir.add("private Double " + simboloCoinApi.getPropiedadVolumen() + ";");
				lineasEscribir.add("");
				lineasEscribir.add("private Double " + simboloCoinApi.getPropiedadVolumen() + "Min;");
				lineasEscribir.add("");
				lineasEscribir.add("private Double " + simboloCoinApi.getPropiedadVolumen() + "Max;");
				lineasEscribir.add("");
			}
			lineasEscribir.add("public VolumenBTC(Timedata td) {");
			lineasEscribir.add("	super(td);");
			lineasEscribir.add("}");
			lineasEscribir.add("}");
			try {
				FileUtils.writeLines(new File("C:\\temp\\VOLUMEN\\" + nombreArchivo), lineasEscribir);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		assertEquals(0, 0);

	}
	
	
}
