package com.criptovolumen.constructor;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.coinapi.entidades.Timedata;
import com.coinapi.enums.Period_identifier;
import com.criptovolumen.enums.SimbolosCoinApi;
import com.criptovolumen.excepciones.ExcepcionExchange;
import com.criptovolumen.servicios.exchanges.impl.coinbene.CoinbeneSv;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { ApplicationContext.class, CoinbeneSv.class })
@EnableConfigurationProperties
public class CoinApiTests {

	Logger logger = LoggerFactory.getLogger(CoinApiTests.class);

	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private CoinbeneSv coinbeneSv;
	
	@Test
	public void asignarScheduledACadaSimboloCoinApi() {
		
		for (SimbolosCoinApi simbolo : SimbolosCoinApi.values()) {
			logger.info(simbolo.getCodigo());
		}
	}
	
	@Test
	public void testExcepcionNotificadaPorTelegram() {
		ExcepcionExchange excepcion = new ExcepcionExchange(context, new Exception("PRUEBA TEST: testExcepcionNotificadaPorTelegram"));
		logger.error("EXCEPCION {}", excepcion.getMessage());
	}
	
	@Test
	public void testCoinBene() {
		//https://a.coinbene.com/market/tradepair/tradeview/kline/history?symbol=BTCUSDT&resolution=15&from=1541344115&to=1541353115
		Calendar calendar = Calendar.getInstance();
		calendar.set(2018, 10, 7, 19, 15);
		Long segundos = calendar.getTimeInMillis() / 1000;
		logger.info(segundos.toString());
		try {
			List<Timedata> resultados = coinbeneSv.getOHLC("BTCUSDT", Period_identifier._15MIN, calendar.toInstant(), Calendar.getInstance().toInstant());
			resultados.forEach(System.out::println);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
//	@Test
//	public void testHayConexionContraServidorCOINAPI() {
//		ExcepcionExchange excepcion = new ExcepcionExchange(context, new Exception("PRUEBA TEST: testExcepcionNotificadaPorTelegram"));
//		logger.error("EXCEPCION {}", excepcion.getMessage());
//	}
}
